/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps_keys.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:01:50 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/18 09:56:20 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_KEYS_H
# define FPS_KEYS_H

# define K_W 119
# define K_S 115
# define K_A 100
# define K_D 97
# define K_E 101
# define K_ESC 65307
# define K_F1 65470
# define K_F2 65471
# define K_F3 65472
# define K_F4 65473
# define K_F5 65474
# define K_F6 65475
# define K_F7 65476
# define K_F8 65477
# define K_F9 65478
# define K_F10 65479
# define K_F11 65480
# define K_F12 65481

#endif
