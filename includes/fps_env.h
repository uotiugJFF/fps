/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps_env.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 15:05:10 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 18:07:19 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_ENV_H
# define FPS_ENV_H

# ifndef WIN_W
#  define WIN_W 1000
# endif

# ifndef WIN_H
#  define WIN_H 800
# endif

# define TILE 5
# define HEIGHT 5
# define FOV 60.0
# define NEAR_CLIP 0.1
# define FAR_CLIP 1000

typedef struct s_texture_set
{
	struct s_texture	*north;
	struct s_texture	*south;
	struct s_texture	*east;
	struct s_texture	*west;
	struct s_texture	*floor;
	struct s_texture	*ceiling;
	struct s_texture	*map_n;
	struct s_texture	*gun;
	struct s_texture	*enemy;
}	t_texture_set;

typedef struct s_keys
{
	int				w;
	int				a;
	int				s;
	int				d;
	struct s_vec3	mouse;
}	t_keys;

typedef struct s_move
{
	int	next_y_n;
	int	next_y_s;
	int	next_x_e;
	int	next_x_w;
	int	prev_y_n;
	int	prev_y_s;
	int	prev_x_e;
	int	prev_x_w;
}	t_move;

typedef struct s_player
{
	struct s_vec3	pos;
	struct s_keys	keys;
	int				shoot;
	long			shoot_time;
	int				pv;
	int				pv_max;
	int				invincible;
	int				invincible_time;
	long			invincible_until;
}	t_player;

typedef struct s_debug
{
	int	infos;
	int	dda;
	int	z;
	int	uv;
	int	light;
	int	wire;
	int	threads;
}	t_debug;

typedef struct s_thread_args
{
	struct s_env	*env;
	struct s_dlist	*list;
}	t_thread_args;

typedef struct s_threads
{
	pthread_t				clip[300];
	struct s_thread_args	clip_args[300];
	int						clip_threads;
}	t_threads;

typedef struct s_env
{
	void					*mlx;
	void					*win;
	struct s_debug			debug;
	struct s_img			img;
	struct s_texture_set	tex_set;
	struct s_map			map;
	struct s_player			player;
	struct s_mat4x4			proj_mat;
	struct s_threads		threads;
	float					z_buff[WIN_H][WIN_W];
	pthread_mutex_t			pxl_mutex[WIN_W];
	long					delta_time;
	int						render_map;
}	t_env;

#endif
