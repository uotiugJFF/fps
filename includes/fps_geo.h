/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps_geo.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 23:34:37 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 20:11:54 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_GEO_H
# define FPS_GEO_H

typedef struct s_vec3
{
	float	x;
	float	y;
	float	z;
	float	w;
}	t_vec3;

typedef struct s_vec2
{
	float	u;
	float	v;
	float	w;
}	t_vec2;

typedef struct s_vec2i
{
	int	x;
	int	y;
}	t_vec2i;

typedef struct s_tri_pts
{
	int	a;
	int	b;
	int	c;
}	t_tri_pts;

typedef struct s_mat4x4
{
	float	m[4][4];
}	t_mat4x4;

typedef struct s_triangle
{
	struct s_vec3		p[3];
	struct s_vec2		t[3];
	struct s_texture	*tex;
	float				light;
}	t_triangle;

typedef struct s_rgb_clr
{
	int	r;
	int	g;
	int	b;
}	t_rgb_clr;

typedef struct s_clip_infos
{
	struct s_vec3		pl;
	struct s_vec3		norm;
	struct s_vec3		in_pts[3];
	struct s_vec3		out_pts[3];
	struct s_vec2		in_tex[3];
	struct s_vec2		out_tex[3];
	struct s_triangle	tr;
	int					in;
	int					out;
}	t_clip_infos;

typedef struct s_tri_tex
{
	t_vec3	p1;
	t_vec3	p2;
	t_vec2	t1;
	t_vec2	t2;
}	t_tri_tex;

typedef struct s_draw_tex
{
	float	su;
	float	sv;
	float	sw;
	float	eu;
	float	ev;
	float	ew;
	float	u;
	float	v;
	float	w;
	float	tstep;
	float	t;
}	t_draw_tex;

typedef struct s_draw_tri
{
	int		d_y;
	int		d_x;
	float	d_u;
	float	d_v;
	float	d_w;
	float	x_step;
	float	u_step;
	float	v_step;
	float	w_step;
	int		x;
	t_vec3	p1;
	t_vec3	p2;
	t_vec2	t1;
	t_vec2	t2;
}	t_draw_tri;

typedef struct s_draw_tex_tri
{
	t_draw_tri	t1;
	t_draw_tri	t2;
	t_draw_tex	d;
}	t_draw_tex_tri;

typedef struct s_door_mesh
{
	t_vec3	s;
	t_vec3	e;
	char	pos;
}	t_door_mesh;

void		round_vec3(t_vec3 *pt);
void		del_tri_dlist(void *data);
void		normalize_vec3(t_vec3 *v);
void		round_tri(t_triangle *tr);
void		matrix_init(t_mat4x4 *mat);
void		ft_vec2swap(t_vec2 *a, t_vec2 *b);
void		ft_vec3swap(t_vec3 *a, t_vec3 *b);
void		tri_mat_mult_vec3(t_triangle i, t_triangle *o, t_mat4x4 m);

float		rad2deg(float rad);
float		len_vec3(t_vec3 vec);
float		deg2rad(float angle);
float		dot_product(t_vec3 a, t_vec3 b);

t_vec2		vec2(float u, float v);

t_vec3		get_normal(t_triangle *tr);
t_vec3		div_vec3(t_vec3 a, float k);
t_vec3		add_vec3(t_vec3 a, t_vec3 b);
t_vec3		mult_vec3(t_vec3 a, float k);
t_vec3		sub_vec3(t_vec3 a, t_vec3 b);
t_vec3		rotate_y(t_vec3 p, int angle);
t_vec3		vec3(float x, float y, float z);
t_vec3		cross_product(t_vec3 a, t_vec3 b);
t_vec3		mat_mult_vec3(t_mat4x4 m, t_vec3 i);

t_mat4x4	mat_rotate_x(float angle);
t_mat4x4	mat_rotate_z(float angle);
t_mat4x4	mat_rotate_y(float angle);
t_mat4x4	matrix_quick_inverse(t_mat4x4 m);
t_mat4x4	mat_project(float fov, float near, float far);
t_mat4x4	matrix_point_at(t_vec3 pos, t_vec3 target, t_vec3 up);

t_tri_pts	pt(int a, int b, int c);

t_triangle	tri(t_vec3 p1, t_vec3 p2, t_vec3 p3);

#endif
