/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/02 13:20:58 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 08:42:28 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_H
# define FPS_H

# include "../libft_2.0/libft.h"
# include "../minilibx/mlx.h"

# include <sys/time.h>
# include <stdlib.h>
# include <math.h>
# include <fcntl.h>
# include <pthread.h>

# include "fps_keys.h"
# include "fps_img.h"
# include "fps_geo.h"
# include "fps_map.h"
# include "fps_env.h"
# include "fps_errors.h"

# include <stdio.h>

int			render(t_env *env);
int			print_err(int err);
int			close_win(t_env *env);
int			clr_from_z(t_vec3 uvw);
int			clr_from_uv(t_vec3 uvw);
int			get_fps(long t1, long t2);
int			int_from_rgb(t_rgb_clr clr);
int			sort_node(void *a, void *b);
int			check_map_content(t_env *env);
int			press_key(int keycode, t_env *env);
int			file_parser(char *path, t_env *env);
int			release_key(int keycode, t_env *env);
int			clr_from_tex(t_triangle tr, t_vec3 uvw);
int			mouse_hook(int btn, int x, int y, void *e);
int			check_closed_map(t_map *map, int x, int z);
int			heuristic_distance(t_vec2 start, t_vec2 end);

void		toggle(int *v);
void		init_mlx(t_env *env);
void		debug_dda(t_env *env);
void		del_path_node(void *d);
void		init_hooks(t_env *env);
void		build_mesh(t_env *env);
void		create_map(t_env *env);
void		reset_tile(void *data);
void		render_map(t_env *env);
void		debug_enemy(t_env *env);
void		calc_uvw(t_draw_tex *d);
void		render_mesh(t_env *env);
void		ft_swap(int *a, int *b);
void		move_player(t_env *env);
void		del_map_line(void *data);
void		render_map_n(t_env *env);
void		render_map_bg(t_env *env);
void		to_screen(t_triangle *tr);
void		render_enemies(t_env *env);
void		reset_z_buffer(t_env *env);
void		sort_points(t_triangle *tr);
void		free_split(char **splitted);
void		ft_fswap(float *a, float *b);
void		list_visible_mesh(t_env *env);
void		set_start_dir(t_map *map, char c);
void		clip_top(t_dlist *tr, t_env *env);
void		to_cartesian_space(t_triangle *tr);
void		set_clip_infos(t_clip_infos *infos);
void		wall_tile(int x, int z, t_env *env);
void		clip_bottom(t_dlist *tr, t_env *env);
void		create_projection_matrix(t_env *env);
void		empty_tile(int x, int z, t_env *env);
void		build_doors(t_tile *tile, t_env *env);
void		dda_check_ray(t_dda *dda, t_env *env);
void		init_dda_ray(t_dda *dda, float angle);
void		build_enemies(t_env *env);
void		tex_triangle(t_triangle tr, t_env *env);
void		try_set_texture(char *line, t_env *env);
void		add_floor(t_tile *tile, t_texture *tex);
void		debug_infos(long t1, long t2, t_env *env);
void		init_tri_tex(t_draw_tri *t, t_tri_tex tt);
void		add_ceiling(t_tile *tile, t_texture *tex);
void		detect_top_wall(t_tile *tile, t_env *env);
void		put_pxl(t_env *env, int x, int y, int clr);
void		detect_left_wall(t_tile *tile, t_env *env);
void		set_draw_tex_p2(t_draw_tex_tri *inf, int i);
void		set_draw_tex_p1(t_draw_tex_tri *inf, int i);
void		detect_right_wall(t_tile *tile, t_env *env);
void		do_player_move(t_env *env, t_vec3 next_pos);
void		detect_bottom_wall(t_tile *tile, t_env *env);
void		door_tile(int x, int z, t_env *env, char dir);
void		debug_tile(int x, int y, int clr, t_env *env);
void		init_dda(t_dda *dda, float shift, t_env *env);
void		add_floor_and_ceiling(t_tile *tile, t_env *env);
void		init_line(t_vec3 start, t_vec3 end, t_line *line);
void		draw_triangle(t_triangle tr, int clr, t_env *env);
void		fill_triangle(t_triangle tr, int clr, t_env *env);
void		set_mouse_pos(t_env *env, int x_init, int y_init);
void		draw_a_star_path(int x, int z, int clr, t_env *env);
void		add_wall(t_tile *tile, t_vec3 pts[2], t_texture *tex);
void		init_path(t_path_node *node, int x, int z, t_env *env);
void		draw_line(t_vec3 start, t_vec3 end, int clr, t_env *env);
void		add_keyframes(t_enemy *enemy, t_anim *anim, int nb, ...);
void		check_neighboor(t_a_star_infos *i, t_vec2 pos, t_env *env);
void		build_door_tri_1(t_tile *tile, t_door_mesh dm, t_env *env);
void		build_door_tri_2(t_tile *tile, t_door_mesh dm, t_env *env);
void		a_star(t_vec2 start, t_vec2 end, t_enemy *enemy, t_env *env);
void		put_line_pxl(t_vec3 *start, t_line *line, int clr, t_env *env);
void		set_uvw(t_vec2 *tex, float t, t_clip_infos infos, t_tri_pts pt);
void		init_clip_infos(t_clip_infos *inf, t_vec3 pl, t_vec3 norm,
				t_triangle tr);
void		sel_render(t_draw_tex_tri *inf, t_triangle tr, t_vec2 pos,
				t_env *env);

long		get_timestamp(void);

size_t		ft_strlen_no_space(char *str);

t_vec3		get_target(t_env *env);
t_vec3		get_look_dir(t_env *env);
t_vec3		inter_plane(t_clip_infos inf, t_vec3 s, t_vec3 e, float *t);

t_dlist		*dup_triangle(t_triangle tr);
t_dlist		*create_tri_1_in(t_clip_infos infos);
t_dlist		*create_tri_2_in(t_clip_infos infos);
t_dlist		*thread_clip(t_dlist *tr, t_env *env);
t_dlist		*get_clip_tri(t_vec3 pl, t_vec3 norm, t_triangle tr);

t_rgb_clr	rgb_from_int(int clr);
t_rgb_clr	rgb(int r, int g, int b);

t_texture	*save_texture_path(char	*str);

t_keyframe	keyframe(float time, int value);

t_triangle	triangle(t_vec3 pts[3], t_vec2 uvs[3], t_texture *tex);

t_map_line	*get_line_z(int z, t_map *map);

#endif
