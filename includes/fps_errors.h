/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps_errors.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/07 00:49:34 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/14 11:12:56 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_ERRORS_H
# define FPS_ERRORS_H

# define ARG_ERR 1
# define MAP_PATH_ERR 2
# define MAP_EMPTY_LINE_ERR 3
# define MAP_ARGS_ERR 4
# define MAP_CHAR 5
# define MAP_NO_START 6
# define MAP_NOT_CLOSED 7
# define MALLOC_ERR 8
# define MLX_INIT_ERR 9
# define MLX_WIN_ERR 10
# define MLX_IMG_ERR 11

#endif
