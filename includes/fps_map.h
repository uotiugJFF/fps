/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fps_map.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/07 00:45:34 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 11:48:43 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FPS_MAP_H
# define FPS_MAP_H

# define EMPTY 0
# define WALL 1
# define DOOR 2

# define MAP_RADIUS 90
# define MAP_POS_X 100
# define MAP_POS_Y 100

typedef struct s_dda
{
	struct s_vec2	start_pos;
	struct s_vec2	look_dir;
	struct s_vec2	map_check;
	struct s_vec2	v_unit_step;
	struct s_vec2	v_step;
	struct s_vec2	v_length;
	float			angle;
	float			dist;
	float			max_dist;
	int				wall_found;
}	t_dda;

typedef struct s_tile
{
	int				type;
	int				x;
	int				z;
	int				visible;
	struct s_vec3	door_pos[2];
	float			door_angle;
	int				door_status;
	int				door_dir;
	struct s_dlist	*mesh;
}	t_tile;

typedef struct s_map_line
{
	char	*str;
	int		x_max;
	int		z;
}	t_map_line;

typedef struct s_keyfrme
{
	long	time;
	int		value;
}	t_keyframe;

typedef struct s_anim
{
	int			max_tile;
	long		start;
	t_keyframe	keyframes[16];
	int			nb_keyframes;
	int			cur_key;
}	t_anim;

typedef struct s_enemy
{
	struct s_vec3	pos;
	struct s_vec3	look_dir;
	struct s_dlist	*mesh;
	int				visible;
	int				reverse_uv;
	int				x_tile;
	int				y_tile;
	int				cur_anim;
	int				dead;
	long			death_time;
	float			shoot_distance;
	struct s_anim	anim[12];
}	t_enemy;

typedef struct s_a_star_infos
{
	struct s_dlist		*to_test;
	struct s_dlist		*current;
	struct s_path_node	*node;
	struct s_vec2i		start;
	struct s_vec2i		end;
	struct s_enemy		*enemy;
}	t_a_star_infos;

typedef struct s_path_node
{
	int					global_goal;
	int					local_goal;
	int					x;
	int					z;
	int					wall;
	int					visited;
	struct s_path_node	*parent;
}	t_path_node;

typedef struct s_map
{
	struct s_dlist		*lines;
	int					start_x;
	int					start_z;
	int					x_max;
	int					z_max;
	int					dir;
	struct s_tile		**map;
	struct s_path_node	**path;
	struct s_dlist		*visible_mesh;
	struct s_dlist		*clipped_mesh;
	struct s_dlist		*enemies;
}	t_map;

#endif
