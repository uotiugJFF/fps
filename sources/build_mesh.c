/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 23:04:50 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 12:08:21 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	detect_neighbor_wall(t_tile *tile, t_env *env)
{
	if (tile->x > 0)
		detect_left_wall(tile, env);
	if (tile->x < env->map.x_max - 1)
		detect_right_wall(tile, env);
	if (tile->z > 0)
		detect_top_wall(tile, env);
	if (tile->z < env->map.z_max - 1)
		detect_bottom_wall(tile, env);
}

void	add_floor_and_ceiling(t_tile *tile, t_env *env)
{
	add_floor(tile, env->tex_set.floor);
	add_ceiling(tile, env->tex_set.ceiling);
}

void	build_mesh(t_env *env)
{
	int	x;
	int	z;

	z = -1;
	while (++z < env->map.z_max)
	{
		x = -1;
		while (++x < env->map.x_max)
		{
			if (env->map.map[z][x].type == WALL)
				detect_neighbor_wall(&env->map.map[z][x], env);
			else if (env->map.map[z][x].type == DOOR)
			{
				env->map.map[z][x].door_angle = 0;
				env->map.map[z][x].door_status = 0;
		//		build_door(&env->map.map[z][x], env);
			}
			else
				add_floor_and_ceiling(&env->map.map[z][x], env);
		}
	}
}
