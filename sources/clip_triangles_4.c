/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clip_triangles_4.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:04:10 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:53:44 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_dlist	*dup_triangle(t_triangle tr)
{
	t_triangle	*new;

	new = malloc(sizeof(t_triangle));
	if (!new)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	new->p[0] = tr.p[0];
	new->p[1] = tr.p[1];
	new->p[2] = tr.p[2];
	new->t[0] = tr.t[0];
	new->t[1] = tr.t[1];
	new->t[2] = tr.t[2];
	new->light = tr.light;
	new->tex = tr.tex;
	return (ft_dl_new(new));
}

t_dlist	*create_tri_1_in(t_clip_infos infos)
{
	t_triangle	*new;
	float		t;

	new = malloc(sizeof(t_triangle));
	if (!new)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	new->p[0] = infos.in_pts[0];
	new->t[0] = infos.in_tex[0];
	new->p[1] = inter_plane(infos, infos.in_pts[0], infos.out_pts[0], &t);
	set_uvw(&new->t[1], t, infos, pt(0, 0, 0));
	new->p[2] = inter_plane(infos, infos.in_pts[0], infos.out_pts[1], &t);
	set_uvw(&new->t[2], t, infos, pt(1, 0, 0));
	new->light = infos.tr.light;
	new->tex = infos.tr.tex;
	return (ft_dl_new(new));
}

t_triangle	*create_tri_2_in_1(t_clip_infos infos, float *t)
{
	t_triangle	*new_1;

	new_1 = malloc(sizeof(t_triangle));
	if (!new_1)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	new_1->p[0] = infos.in_pts[0];
	new_1->t[0] = infos.in_tex[0];
	new_1->p[1] = infos.in_pts[1];
	new_1->t[1] = infos.in_tex[1];
	new_1->p[2] = inter_plane(infos, infos.in_pts[0], infos.out_pts[0], t);
	set_uvw(&new_1->t[2], *t, infos, pt(0, 0, 0));
	new_1->light = infos.tr.light;
	new_1->tex = infos.tr.tex;
	return (new_1);
}

t_triangle	*create_tri_2_in_2(t_clip_infos infos, t_triangle *new_1, float *t)
{
	t_triangle	*new_2;

	new_2 = malloc(sizeof(t_triangle));
	if (!new_2)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	new_2->p[0] = infos.in_pts[1];
	new_2->t[0] = infos.in_tex[1];
	new_2->p[1] = new_1->p[2];
	new_2->t[1] = new_1->t[2];
	new_2->p[2] = inter_plane(infos, infos.in_pts[1], infos.out_pts[0], t);
	set_uvw(&new_2->t[2], *t, infos, pt(0, 1, 1));
	new_2->light = infos.tr.light;
	new_2->tex = infos.tr.tex;
	return (new_2);
}

t_dlist	*create_tri_2_in(t_clip_infos infos)
{
	t_dlist		*new_list;
	t_triangle	*new_1;
	t_triangle	*new_2;
	float		t;

	new_1 = create_tri_2_in_1(infos, &t);
	new_list = ft_dl_new(new_1);
	new_2 = create_tri_2_in_2(infos, new_1, &t);
	ft_dl_add_back(&new_list, ft_dl_new(new_2));
	return (new_list);
}
