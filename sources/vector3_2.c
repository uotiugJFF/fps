/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector3_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 13:34:53 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:36:08 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_vec3	mult_vec3(t_vec3 a, float k)
{
	t_vec3	r;

	r.x = a.x * k;
	r.y = a.y * k;
	r.z = a.z * k;
	r.w = a.w;
	return (r);
}

t_vec3	div_vec3(t_vec3 a, float k)
{
	t_vec3	r;

	r.x = a.x / k;
	r.y = a.y / k;
	r.z = a.z / k;
	r.w = a.w;
	return (r);
}

t_vec3	add_vec3(t_vec3 a, t_vec3 b)
{
	t_vec3	r;

	r.x = a.x + b.x;
	r.y = a.y + b.y;
	r.z = a.z + b.z;
	r.w = a.w;
	return (r);
}

t_vec3	sub_vec3(t_vec3 a, t_vec3 b)
{
	t_vec3	r;

	r.x = a.x - b.x;
	r.y = a.y - b.y;
	r.z = a.z - b.z;
	r.w = a.w;
	return (r);
}

void	round_vec3(t_vec3 *pt)
{
	pt->x = roundf(pt->x);
	pt->y = roundf(pt->y);
	pt->z = roundf(pt->z);
}
