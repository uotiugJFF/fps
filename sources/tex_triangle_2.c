/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tex_triangle_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 02:37:59 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 09:46:34 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_tri_tex(t_draw_tri *t, t_tri_tex tt)
{
	t->p1 = tt.p1;
	t->p2 = tt.p2;
	t->t1 = tt.t1;
	t->t2 = tt.t2;
	t->d_x = tt.p2.x - tt.p1.x;
	t->d_y = tt.p2.y - tt.p1.y;
	t->d_u = tt.t2.u - tt.t1.u;
	t->d_v = tt.t2.v - tt.t1.v;
	t->d_w = tt.t2.w - tt.t1.w;
	t->x_step = 0;
	t->u_step = 0;
	t->v_step = 0;
	t->w_step = 0;
	if (t->d_y)
	{
		t->x_step = t->d_x / (float)t->d_y;
		t->u_step = t->d_u / (float)t->d_y;
		t->v_step = t->d_v / (float)t->d_y;
		t->w_step = t->d_w / (float)t->d_y;
	}
}

void	set_draw_tex_p1(t_draw_tex_tri *inf, int i)
{
	inf->d.su = inf->t1.t1.u + (float)(i - inf->t1.p1.y) * inf->t1.u_step;
	inf->d.sv = inf->t1.t1.v + (float)(i - inf->t1.p1.y) * inf->t1.v_step;
	inf->d.sw = inf->t1.t1.w + (float)(i - inf->t1.p1.y) * inf->t1.w_step;
	inf->d.eu = inf->t1.t1.u + (float)(i - inf->t1.p1.y) * inf->t2.u_step;
	inf->d.ev = inf->t1.t1.v + (float)(i - inf->t1.p1.y) * inf->t2.v_step;
	inf->d.ew = inf->t1.t1.w + (float)(i - inf->t1.p1.y) * inf->t2.w_step;
	if (inf->t1.x > inf->t2.x)
	{
		ft_swap(&inf->t1.x, &inf->t2.x);
		ft_fswap(&inf->d.su, &inf->d.eu);
		ft_fswap(&inf->d.sv, &inf->d.ev);
		ft_fswap(&inf->d.sw, &inf->d.ew);
	}
	inf->d.u = inf->d.su;
	inf->d.v = inf->d.sv;
	inf->d.w = inf->d.sw;
	if ((float)(inf->t2.x - inf->t1.x) != 0)
		inf->d.tstep = 1.0 / ((float)(inf->t2.x - inf->t1.x));
	inf->d.t = 0.0;
}

void	set_draw_tex_p2(t_draw_tex_tri *inf, int i)
{
	inf->d.su = inf->t1.t2.u + (float)(i - inf->t1.p2.y) * inf->t1.u_step;
	inf->d.sv = inf->t1.t2.v + (float)(i - inf->t1.p2.y) * inf->t1.v_step;
	inf->d.sw = inf->t1.t2.w + (float)(i - inf->t1.p2.y) * inf->t1.w_step;
	inf->d.eu = inf->t1.t1.u + (float)(i - inf->t1.p1.y) * inf->t2.u_step;
	inf->d.ev = inf->t1.t1.v + (float)(i - inf->t1.p1.y) * inf->t2.v_step;
	inf->d.ew = inf->t1.t1.w + (float)(i - inf->t1.p1.y) * inf->t2.w_step;
	if (inf->t1.x > inf->t2.x)
	{
		ft_swap(&inf->t1.x, &inf->t2.x);
		ft_fswap(&inf->d.su, &inf->d.eu);
		ft_fswap(&inf->d.sv, &inf->d.ev);
		ft_fswap(&inf->d.sw, &inf->d.ew);
	}
	inf->d.u = inf->d.su;
	inf->d.v = inf->d.sv;
	inf->d.w = inf->d.sw;
	if ((float)(inf->t2.x - inf->t1.x) != 0)
		inf->d.tstep = 1.0 / ((float)(inf->t2.x - inf->t1.x));
	inf->d.t = 0.0;
}

void	calc_uvw(t_draw_tex *d)
{
	d->u = (1.0f - d->t) * d->su + d->t * d->eu;
	d->v = (1.0f - d->t) * d->sv + d->t * d->ev;
	d->w = (1.0f - d->t) * d->sw + d->t * d->ew;
}

void	sel_render(t_draw_tex_tri *inf, t_triangle tr, t_vec2 pos, t_env *env)
{
	if (env->debug.z)
		put_pxl(env, pos.v, pos.u, clr_from_z(vec3(inf->d.u / inf->d.w,
					inf->d.v / inf->d.w, inf->d.w)));
	else if (env->debug.uv)
		put_pxl(env, pos.v, pos.u, clr_from_uv(vec3(inf->d.u / inf->d.w,
					inf->d.v / inf->d.w, inf->d.w)));
	else
		put_pxl(env, pos.v, pos.u, clr_from_tex(tr,
				vec3(inf->d.u / inf->d.w,
					inf->d.v / inf->d.w, inf->d.w)));
	if ((clr_from_tex(tr, vec3(inf->d.u / inf->d.w, inf->d.v / inf->d.w,
					inf->d.w)) >> 24 & 0xff) == 0)
		env->z_buff[max(0, min(WIN_H - 1, pos.u))][max(0, min(WIN_W
					- 1, pos.v))] = inf->d.w;
}
