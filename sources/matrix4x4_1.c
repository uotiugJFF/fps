/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix4x4_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 14:33:41 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:37:26 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	matrix_init(t_mat4x4 *mat)
{
	int	i;
	int	j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
			mat->m[i][j] = 0;
	}
}

void	create_projection_matrix(t_env *env)
{
	float	aspect_ratio;
	float	fov_rad;

	matrix_init(&env->proj_mat);
	aspect_ratio = (float)WIN_H / (float)WIN_W;
	fov_rad = 1.0 / tan(deg2rad(FOV) / 2.0);
	env->proj_mat.m[0][0] = aspect_ratio * fov_rad;
	env->proj_mat.m[1][1] = fov_rad;
	env->proj_mat.m[2][2] = (float)FAR_CLIP / (float)(FAR_CLIP - NEAR_CLIP);
	env->proj_mat.m[2][3] = 1.0;
	env->proj_mat.m[3][2] = (float)(-FAR_CLIP * NEAR_CLIP)
		/ (float)(FAR_CLIP - NEAR_CLIP);
}

t_mat4x4	matrix_set_pt_at(t_vec3 pos, t_vec3 right, t_vec3 up, t_vec3 frwd)
{
	t_mat4x4	mat;

	mat.m[0][0] = right.x;
	mat.m[0][1] = right.y;
	mat.m[0][2] = right.z;
	mat.m[0][3] = 0;
	mat.m[1][0] = up.x;
	mat.m[1][1] = up.y;
	mat.m[1][2] = up.z;
	mat.m[1][3] = 0;
	mat.m[2][0] = frwd.x;
	mat.m[2][1] = frwd.y;
	mat.m[2][2] = frwd.z;
	mat.m[2][3] = 0;
	mat.m[3][0] = pos.x;
	mat.m[3][1] = pos.y;
	mat.m[3][2] = pos.z;
	mat.m[3][3] = 1;
	return (mat);
}

t_mat4x4	matrix_point_at(t_vec3 pos, t_vec3 target, t_vec3 up)
{
	t_vec3	new_forward;
	t_vec3	a;
	t_vec3	new_up;
	t_vec3	new_right;

	new_forward = sub_vec3(target, pos);
	normalize_vec3(&new_forward);
	a = mult_vec3(new_forward, dot_product(up, new_forward));
	new_up = sub_vec3(up, a);
	normalize_vec3(&new_up);
	new_right = cross_product(new_up, new_forward);
	return (matrix_set_pt_at(pos, new_right, new_up, new_forward));
}

t_mat4x4	matrix_quick_inverse(t_mat4x4 m)
{
	t_mat4x4	mat;

	mat.m[0][0] = m.m[0][0];
	mat.m[0][1] = m.m[1][0];
	mat.m[0][2] = m.m[2][0];
	mat.m[0][3] = 0;
	mat.m[1][0] = m.m[0][1];
	mat.m[1][1] = m.m[1][1];
	mat.m[1][2] = m.m[2][1];
	mat.m[1][3] = 0;
	mat.m[2][0] = m.m[0][2];
	mat.m[2][1] = m.m[1][2];
	mat.m[2][2] = m.m[2][2];
	mat.m[2][3] = 0;
	mat.m[3][0] = -(m.m[3][0] * mat.m[0][0] + m.m[3][1] * mat.m[1][0]
			+ m.m[3][2] * mat.m[2][0]);
	mat.m[3][1] = -(m.m[3][0] * mat.m[0][1] + m.m[3][1] * mat.m[1][1]
			+ m.m[3][2] * mat.m[2][1]);
	mat.m[3][2] = -(m.m[3][0] * mat.m[0][2] + m.m[3][1] * mat.m[1][2]
			+ m.m[3][2] * mat.m[2][2]);
	mat.m[3][3] = 1;
	return (mat);
}
