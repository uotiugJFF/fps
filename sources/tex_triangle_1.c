/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tex_triangle_1.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 00:07:57 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 12:47:15 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_tri_tex	tri_tex(t_vec3 p1, t_vec3 p2, t_vec2 t1, t_vec2 t2)
{
	t_tri_tex	tt;

	tt.p1 = p1;
	tt.p2 = p2;
	tt.t1 = t1;
	tt.t2 = t2;
	return (tt);
}

void	draw_tex_tri_p1(t_triangle tr, t_draw_tex_tri *inf, t_env *env)
{
	int	i;
	int	j;

	i = tr.p[0].y;
	while (i < tr.p[1].y)
	{
		inf->t1.x = tr.p[0].x + (inf->t1.x_step) * (float)(i - tr.p[0].y);
		inf->t2.x = tr.p[0].x + (inf->t2.x_step) * (float)(i - tr.p[0].y);
		set_draw_tex_p1(inf, i);
		j = inf->t1.x;
		while (j < inf->t2.x)
		{
			calc_uvw(&inf->d);
			pthread_mutex_lock(&env->pxl_mutex[max(0, min(i, WIN_W))]);
			if (inf->d.w > env->z_buff[i][j])
				sel_render(inf, tr, vec2(i, j), env);
			pthread_mutex_unlock(&env->pxl_mutex[max(0, min(i, WIN_W))]);
			inf->d.t += inf->d.tstep;
			j++;
		}
		i++;
	}
}

void	set_inf_p2(t_draw_tex_tri *inf, t_triangle tr)
{
	inf->t1.p1 = tr.p[0];
	inf->t1.t1 = tr.t[0];
	inf->t1.p2 = tr.p[1];
	inf->t1.t2 = tr.t[1];
}

void	draw_tex_tri_p2(t_triangle tr, t_draw_tex_tri *inf, t_env *env)
{
	int	i;
	int	j;

	i = tr.p[1].y;
	while (i < tr.p[2].y)
	{
		inf->t1.x = tr.p[1].x + inf->t1.x_step * (float)(i - tr.p[1].y);
		inf->t2.x = tr.p[0].x + inf->t2.x_step * (float)(i - tr.p[0].y);
		set_inf_p2(inf, tr);
		set_draw_tex_p2(inf, i);
		j = inf->t1.x;
		while (j < inf->t2.x)
		{
			calc_uvw(&inf->d);
			pthread_mutex_lock(&env->pxl_mutex[max(0, min(i, WIN_W))]);
			if (inf->d.w > env->z_buff[i][j])
				sel_render(inf, tr, vec2(i, j), env);
			pthread_mutex_unlock(&env->pxl_mutex[max(0, min(i, WIN_W))]);
			inf->d.t += inf->d.tstep;
			j++;
		}
		i++;
	}
}

void	tex_triangle(t_triangle tr, t_env *env)
{
	t_draw_tex_tri	inf;

	sort_points(&tr);
	init_tri_tex(&inf.t1, tri_tex(tr.p[0], tr.p[1], tr.t[0], tr.t[1]));
	init_tri_tex(&inf.t2, tri_tex(tr.p[0], tr.p[2], tr.t[0], tr.t[2]));
	if (inf.t1.d_y)
		draw_tex_tri_p1(tr, &inf, env);
	init_tri_tex(&inf.t1, tri_tex(tr.p[1], tr.p[2], tr.t[1], tr.t[2]));
	if (inf.t1.d_y)
		draw_tex_tri_p2(tr, &inf, env);
}
