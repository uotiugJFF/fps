/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_triangle_1.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:26:41 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 19:11:17 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	fill_bottom_tri(t_triangle tr, int clr, t_env *env)
{
	t_line	l1;
	t_line	l2;
	t_vec3	p4;
	t_vec3	f1;
	t_vec3	f2;

	round_tri(&tr);
	p4 = tr.p[2];
	init_line(tr.p[2], tr.p[0], &l1);
	init_line(p4, tr.p[1], &l2);
	f1 = tr.p[2];
	f2 = p4;
	while (!(tr.p[2].x == tr.p[0].x && tr.p[2].y == tr.p[0].y)
		|| !(p4.x == tr.p[1].x && p4.y == tr.p[1].y))
	{
		while (f1.y == tr.p[2].y && !(tr.p[2].x == tr.p[0].x
				&& tr.p[2].y == tr.p[0].y))
			put_line_pxl(&tr.p[2], &l1, clr, env);
		while (f2.y == p4.y && !(p4.x == tr.p[1].x && p4.y == tr.p[1].y))
			put_line_pxl(&p4, &l2, clr, env);
		draw_line(tr.p[2], p4, clr, env);
		f1 = tr.p[2];
		f2 = p4;
	}
}

void	fill_top_tri(t_triangle tr, int clr, t_env *env)
{
	t_line	l1;
	t_line	l2;
	t_vec3	p4;
	t_vec3	f1;
	t_vec3	f2;

	round_tri(&tr);
	p4 = tr.p[0];
	init_line(tr.p[0], tr.p[1], &l1);
	init_line(p4, tr.p[2], &l2);
	f1 = tr.p[0];
	f2 = p4;
	while (!(tr.p[1].x == tr.p[0].x && tr.p[1].y == tr.p[0].y)
		|| !(tr.p[2].x == p4.x && tr.p[2].y == p4.y))
	{
		while (f1.y == tr.p[0].y && !(tr.p[1].x == tr.p[0].x
				&& tr.p[1].y == tr.p[0].y))
			put_line_pxl(&tr.p[0], &l1, clr, env);
		while (f2.y == p4.y && !(tr.p[2].x == p4.x && tr.p[2].y == p4.y))
			put_line_pxl(&p4, &l2, clr, env);
		draw_line(tr.p[0], p4, clr, env);
		f1 = tr.p[0];
		f2 = p4;
	}
}

void	fill_triangle(t_triangle tr, int clr, t_env *env)
{
	t_vec3		p4;
	float		x;
	t_triangle	tr1;
	t_triangle	tr2;

	sort_points(&tr);
	if (tr.p[0].y == tr.p[1].y)
		fill_bottom_tri(tr, clr, env);
	else if (tr.p[1].y == tr.p[2].y)
		fill_top_tri(tr, clr, env);
	else
	{
		x = tr.p[0].x + (tr.p[0].y - tr.p[1].y) / (tr.p[0].y - tr.p[2].y)
			* (tr.p[2].x - tr.p[0].x);
		p4 = vec3(x, tr.p[1].y, 0);
		tr1 = tri(p4, tr.p[1], tr.p[2]);
		fill_bottom_tri(tr1, clr, env);
		tr2 = tri(tr.p[0], tr.p[1], p4);
		fill_top_tri(tr2, clr, env);
	}
}
