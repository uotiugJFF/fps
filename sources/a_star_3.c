/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_star_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 08:48:05 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 09:47:47 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	heuristic_distance(t_vec2 start, t_vec2 end)
{
	return ((start.u - end.u) * (start.u - end.u)
		+ (start.v - end.v) * (start.v - end.v));
}

int	sort_node(void *a, void *b)
{
	return (((t_path_node *)a)->global_goal < ((t_path_node *)b)->global_goal);
}

void	del_path_node(void *d)
{
	// peut etre plus de truc a free ici;
	(void)d;
}
