/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 19:38:41 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 19:06:17 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_line(t_vec3 start, t_vec3 end, t_line *line)
{
	line->dx = fabs(end.x - start.x);
	line->sx = 1;
	if (start.x > end.x)
		line->sx = -1;
	line->dy = -fabs(end.y - start.y);
	line->sy = 1;
	if (start.y > end.y)
		line->sy = -1;
	line->err = line->dx + line->dy;
}

void	put_line_pxl(t_vec3 *start, t_line *line, int clr, t_env *env)
{
	put_pxl(env, start->x, start->y, clr);
	line->err2 = 2 * line->err;
	if (line->err2 >= line->dy)
	{
		line->err += line->dy;
		start->x += line->sx;
	}
	if (line->err2 <= line->dx)
	{
		line->err += line->dx;
		start->y += line->sy;
	}
}

void	draw_line(t_vec3 start, t_vec3 end, int clr, t_env *env)
{
	t_line	line;

	start.x = (int)start.x;
	start.y = (int)start.y;
	end.x = (int)end.x;
	end.y = (int)end.y;
	init_line(start, end, &line);
	while (!((int)start.x == (int)end.x && (int)start.y == (int)end.y))
		put_line_pxl(&start, &line, clr, env);
}
