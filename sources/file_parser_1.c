/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parser_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 15:17:38 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/14 09:50:12 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_texture_set(t_env *env)
{
	env->tex_set.north = NULL;
	env->tex_set.south = NULL;
	env->tex_set.east = NULL;
	env->tex_set.west = NULL;
	env->tex_set.floor = NULL;
	env->tex_set.ceiling = NULL;
}

int	all_textures_set(t_env *env)
{
	if (env->tex_set.north && env->tex_set.south
		&& env->tex_set.east && env->tex_set.west
		&& env->tex_set.floor && env->tex_set.ceiling)
	{
		return (1);
	}
	return (0);
}

void	save_map_line(char *line, int *empty_line, t_env *env)
{
	char		*clean_line;
	t_map_line	*map_line;

	clean_line = ft_strtrim(line, "\n");
	if (ft_strlen_no_space(line) == 0)
	{
		if (env->map.lines != NULL && *empty_line == 0)
			*empty_line = 1;
	}
	else if (*empty_line != 2)
	{
		if (*empty_line)
			*empty_line = 2;
		map_line = malloc(sizeof(t_map_line));
		if (!map_line)
		{
			// il faut gere les erreurs ici
			return ;
		}
		map_line->str = ft_strdup(clean_line);
		map_line->x_max = ft_strlen(clean_line);
		ft_dl_add_back(&env->map.lines, ft_dl_new(map_line));
	}
	free(clean_line);
}

int	file_parser(char *path, t_env *env)
{
	int		fd;
	char	*line;
	int		empty_line;

	empty_line = 0;
	fd = open(path, O_RDONLY);
	if (fd < 0)
		return (print_err(MAP_PATH_ERR));
	init_texture_set(env);
	env->map.lines = NULL;
	line = get_next_line(fd);
	while (line)
	{
		if (!all_textures_set(env))
			try_set_texture(line, env);
		else
			save_map_line(line, &empty_line, env);
		free(line);
		line = get_next_line(fd);
	}
	if (!all_textures_set(env))
		return (print_err(MAP_ARGS_ERR));
	if (empty_line == 2)
		return (print_err(MAP_EMPTY_LINE_ERR));
	return (check_map_content(env));
}
