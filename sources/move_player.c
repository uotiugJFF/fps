/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 15:36:56 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/19 15:45:11 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	set_mouse_pos(t_env *env, int x_init, int y_init)
{
	int	x;
	int	y;

	mlx_mouse_get_pos(env->mlx, env->win, &x, &y);
	env->player.keys.mouse.x = (int)(env->player.keys.mouse.x
			- x_init + x) % 1800;
	env->player.keys.mouse.y = min(100, max(-100, env->player.keys.mouse. y
				+ y_init - y));
	if (x > WIN_W - 100 || x < 100 || y < 100 || y > WIN_H - 100)
		mlx_mouse_move(env->mlx, env->win, WIN_W / 2, WIN_H / 2);
}

void	init_moves(t_move *mv, t_vec3 next_pos, t_env *env)
{
	float	char_size;

	char_size = 0.05;
	mv->next_y_n = -next_pos.z / TILE - char_size;
	mv->next_y_s = -next_pos.z / TILE + char_size;
	mv->next_x_e = next_pos.x / TILE - char_size;
	mv->next_x_w = next_pos.x / TILE + char_size;
	mv->prev_y_n = -env->player.pos.z / TILE - char_size;
	mv->prev_y_s = -env->player.pos.z / TILE + char_size;
	mv->prev_x_e = env->player.pos.x / TILE - char_size;
	mv->prev_x_w = env->player.pos.x / TILE + char_size;
}

int	is_walkable(t_tile tile)
{
	if (tile.type == 0 || (tile.type == 2 && tile.door_status == 2))
		return (1);
	return (0);
}

void	do_player_move(t_env *env, t_vec3 next_pos)
{
	t_move	mv;

	init_moves(&mv, next_pos, env);
	if (is_walkable(env->map.map[mv.next_y_n][mv.next_x_e])
			&& is_walkable(env->map.map[mv.next_y_s][mv.next_x_e])
			&& is_walkable(env->map.map[mv.next_y_n][mv.next_x_w])
			&& is_walkable(env->map.map[mv.next_y_s][mv.next_x_w]))
		env->player.pos = next_pos;
	else if (is_walkable(env->map.map[mv.prev_y_n][mv.next_x_e])
			&& is_walkable(env->map.map[mv.prev_y_s][mv.next_x_e])
			&& is_walkable(env->map.map[mv.prev_y_n][mv.next_x_w])
			&& is_walkable(env->map.map[mv.prev_y_s][mv.next_x_w]))
		env->player.pos = vec3(next_pos.x, next_pos.y, env->player.pos.z);
	else if (is_walkable(env->map.map[mv.next_y_n][mv.prev_x_e])
			&& is_walkable(env->map.map[mv.next_y_s][mv.prev_x_e])
			&& is_walkable(env->map.map[mv.next_y_n][mv.prev_x_w])
			&& is_walkable(env->map.map[mv.next_y_s][mv.prev_x_w]))
		env->player.pos = vec3(env->player.pos.x, next_pos.y, next_pos.z);
}
