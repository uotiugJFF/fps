/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:24:21 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 13:34:10 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	del_map_line(void *data)
{
	free(((t_map_line *)data)->str);
}

void	init_tile(int x, int z, t_env *env)
{
	env->map.map[z][x].x = x;
	env->map.map[z][x].z = z;
	env->map.map[z][x].visible = 0;
	env->map.map[z][x].mesh = NULL;
}

void	empty_tile(int x, int z, t_env *env)
{
	init_tile(x, z, env);
	env->map.map[z][x].type = EMPTY;
}

void	door_tile(int x, int z, t_env *env, char dir)
{
	init_tile(x, z, env);
	env->map.map[z][x].type = DOOR;
	if (dir == 'P')
		env->map.map[z][x].door_dir = 0;
	else if (dir == 'O')
		env->map.map[z][x].door_dir = -90;
	else if (dir == 'I')
		env->map.map[z][x].door_dir = 180;
	else if (dir == 'U')
		env->map.map[z][x].door_dir = 90;
}

void	wall_tile(int x, int z, t_env *env)
{
	init_tile(x, z, env);
	env->map.map[z][x].type = WALL;
}
