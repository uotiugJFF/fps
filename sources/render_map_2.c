/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_map_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 20:11:42 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 20:16:52 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	render_map_bg(t_env *env)
{
	int	x;
	int	y;

	x = MAP_POS_X - MAP_RADIUS - 1;
	while (++x < MAP_POS_X + MAP_RADIUS)
	{
		y = MAP_POS_Y - MAP_RADIUS - 1;
		while (++y < MAP_POS_Y + MAP_RADIUS)
		{
			if ((x - MAP_POS_X) * (x - MAP_POS_X) + (y - MAP_POS_Y)
				* (y - MAP_POS_Y) < (MAP_RADIUS * MAP_RADIUS))
				put_pxl(env, x, y, 0x333333);
		}
	}
}

void	render_n_img(t_texture *tex, int x, int y, t_env *env)
{
	int	i;
	int	j;
	int	clr;
	int	alpha;

	i = -1;
	while (++i < tex->width)
	{
		j = -1;
		while (++j < tex->height)
		{
			clr = (*(int *)((tex->img.addr) + (4 * tex->width * i) + (4 * j)));
			alpha = (clr >> 24 & 0xFF);
			if (alpha == 0)
				put_pxl(env, x + j - tex->width / 2,
					y + (tex->height - i) - tex->height / 2, clr);
		}
	}
}

void	render_map_n(t_env *env)
{
	float	angle;

	angle = -env->map.dir + env->player.keys.mouse.x / 5.0 + 90;
	render_n_img(env->tex_set.map_n, MAP_POS_X + MAP_RADIUS
		* cos(deg2rad(angle)), MAP_POS_Y + MAP_RADIUS * sin(deg2rad(angle)),
		env);
}
