/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   closed_map_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 18:15:53 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 12:26:27 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	check_closed_right(t_map *map, int x, int z)
{
	t_map_line	*line;

	line = get_line_z(z, map);
	if (!line)
		return (0);
	if (x + 1 >= line->x_max)
		return (0);
	if (line->str[x + 1] == '0')
	{
		line->str[x + 1] = 'o';
		if (!check_closed_map(map, x + 1, z))
			return (0);
	}
	else if (line->str[x + 1] == ' ')
		return (0);
	return (1);
}

int	check_closed_left(t_map *map, int x, int z)
{
	t_map_line	*line;

	line = get_line_z(z, map);
	if (!line)
		return (0);
	if (x - 1 < 0)
		return (0);
	if (line->str[x - 1] == '0')
	{
		line->str[x - 1] = 'o';
		if (!check_closed_map(map, x - 1, z))
			return (0);
	}
	else if (line->str[x - 1] == ' ')
		return (0);
	return (1);
}

int	check_closed_down(t_map *map, int x, int z)
{
	t_map_line	*line;

	line = get_line_z(z + 1, map);
	if (!line)
		return (0);
	if (x > line->x_max)
		return (0);
	if (line->str[x] == '0')
	{
		line->str[x] = 'o';
		if (!check_closed_map(map, x, z + 1))
			return (0);
	}
	else if (line->str[x] == ' ')
		return (0);
	return (1);
}

int	check_closed_up(t_map *map, int x, int z)
{
	t_map_line	*line;

	line = get_line_z(z - 1, map);
	if (!line)
		return (0);
	if (x > line->x_max)
		return (0);
	if (line->str[x] == '0')
	{
		line->str[x] = 'o';
		if (!check_closed_map(map, x, z - 1))
			return (0);
	}
	else if (line->str[x] == ' ')
		return (0);
	return (1);
}

int	check_closed_map(t_map *map, int x, int z)
{
	if (!check_closed_right(map, x, z))
		return (0);
	if (!check_closed_left(map, x, z))
		return (0);
	if (!check_closed_down(map, x, z))
		return (0);
	if (!check_closed_up(map, x, z))
		return (0);
	return (1);
}
