/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parser_3.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 18:14:12 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/13 18:14:25 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_texture	*save_texture_path(char	*str)
{
	t_texture	*new;

	new = malloc(sizeof(t_texture));
	new->path = str;
	new->width = 0;
	new->height = 0;
	return (new);
}

void	try_save_texture(char *str, char *name, t_texture **tex)
{
	char	**splitted;
	char	*clean_path;

	if (ft_strncmp(str, name, 2) == 0)
	{
		splitted = ft_split(str, ' ');
		if (splitted[0] && ft_strlen(splitted[0]) != 2)
		{
			free_split(splitted);
			return ;
		}
		if (splitted[1])
		{
			clean_path = ft_strtrim(&str[2], " ");
			*tex = save_texture_path(ft_strdup(clean_path));
			free(clean_path);
		}
		free_split(splitted);
	}
}

void	try_set_texture(char *line, t_env *env)
{
	char	*clean_line;

	clean_line = ft_strtrim(line, " \n");
	if (!env->tex_set.north)
		try_save_texture(clean_line, "NO", &env->tex_set.north);
	if (!env->tex_set.south)
		try_save_texture(clean_line, "SO", &env->tex_set.south);
	if (!env->tex_set.east)
		try_save_texture(clean_line, "EA", &env->tex_set.east);
	if (!env->tex_set.west)
		try_save_texture(clean_line, "WE", &env->tex_set.west);
	if (!env->tex_set.floor)
		try_save_texture(clean_line, "FL", &env->tex_set.floor);
	if (!env->tex_set.ceiling)
		try_save_texture(clean_line, "CE", &env->tex_set.ceiling);
	free(clean_line);
}

void	set_start_dir(t_map *map, char c)
{
	if (c == 'N')
		map->dir = 0;
	else if (c == 'S')
		map->dir = 180;
	else if (c == 'E')
		map->dir = -90;
	else
		map->dir = 90;
}
