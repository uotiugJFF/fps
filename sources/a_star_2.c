/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_star_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 08:47:31 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 14:11:47 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_path(t_path_node *node, int x, int z, t_env *env)
{
	node->global_goal = INT_MAX;
	node->local_goal = INT_MAX;
	node->x = x;
	node->z = z;
	node->wall = env->map.map[z][x].type == WALL;
	node->visited = 0;
	node->parent = NULL;
}

void	draw_a_star_path(int x, int z, int clr, t_env *env)
{
	t_vec3	start;
	t_vec3	end;
	int		pxl_x;
	int		pxl_y;

	start.x = WIN_W / 2.0 - (env->map.x_max * TILE) + x * TILE * 2;
	start.y = WIN_H / 2.0 - (env->map.z_max * TILE) + env->map.z_max * 2 * TILE
		- (z + 1) * TILE * 2 + 1;
	end.x = WIN_W / 2.0 - (env->map.x_max * TILE) + (x + 1) * TILE * 2;
	end.y = WIN_H / 2.0 - (env->map.z_max * TILE) + env->map.z_max * 2 * TILE
		- z * TILE * 2 + 1;
	pxl_x = start.x - 1;
	while (++pxl_x < end.x)
	{
		pxl_y = start.y - 1;
		while (++pxl_y < end.y)
		{
			if (pxl_x < start.x + 2 || pxl_x > end.x - 3
				|| pxl_y < start.y + 2 || pxl_y > end.y - 3)
				put_pxl(env, pxl_x, pxl_y, clr);
		}
	}
}

int	a_star_obstacle(t_vec2 pos, t_env *env)
{
	t_vec2i	p;

	p.x = pos.u;
	p.y = pos.v;
	return (env->map.map[p.y][p.x].type == WALL
		|| (env->map.map[p.y][p.x].type == DOOR
			&& env->map.map[p.y][p.x].door_status != 2));
}

void	check_neighboor(t_a_star_infos *i, t_vec2 pos, t_env *env)
{
	t_path_node	*neighboor;
	float		possible_lower_goal;

	neighboor = &env->map.path[(int)pos.v][(int)pos.u];
	if (!neighboor->visited && !a_star_obstacle(pos, env))
		ft_dl_add_back(&i->to_test, ft_dl_new(neighboor));
	possible_lower_goal = i->node->local_goal
		+ heuristic_distance(vec2(i->node->x, i->node->z),
			vec2(neighboor->x, neighboor->z));
	if (possible_lower_goal < neighboor->local_goal)
	{
		neighboor->parent = i->node;
		neighboor->local_goal = possible_lower_goal;
		neighboor->global_goal = neighboor->local_goal
			+ heuristic_distance(vec2(neighboor->x, neighboor->z),
				vec2(i->end.x, i->end.y));
	}
}
