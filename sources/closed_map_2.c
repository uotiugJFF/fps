/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   closed_map_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:26:35 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 12:26:49 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_map_line	*get_line_z(int z, t_map *map)
{
	t_dlist	*tmp;

	tmp = map->lines;
	while (tmp && ((t_map_line *)(tmp->data))->z != z)
		tmp = tmp->next;
	if (tmp)
		return (tmp->data);
	return (NULL);
}
