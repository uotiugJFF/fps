/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parser_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 18:09:40 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 11:49:09 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	add_enemy_keyframes(t_enemy *enemy)
{
	add_keyframes(enemy, &enemy->anim[0], 4,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3));
	add_keyframes(enemy, &enemy->anim[1], 3,
		keyframe(0.5, 0), keyframe(0.1, 1), keyframe(0.8, 0));
	add_keyframes(enemy, &enemy->anim[2], 4,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3));
	add_keyframes(enemy, &enemy->anim[3], 3,
		keyframe(0.5, 0), keyframe(0.1, 1), keyframe(0.8, 0));
	add_keyframes(enemy, &enemy->anim[4], 4,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3));
	add_keyframes(enemy, &enemy->anim[5], 3,
		keyframe(0.5, 0), keyframe(0.1, 1), keyframe(0.8, 0));
	add_keyframes(enemy, &enemy->anim[6], 4,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3));
	add_keyframes(enemy, &enemy->anim[7], 3,
		keyframe(0.5, 0), keyframe(0.1, 1), keyframe(0.8, 0));
	add_keyframes(enemy, &enemy->anim[8], 4,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3));
	add_keyframes(enemy, &enemy->anim[9], 3,
		keyframe(0.5, 0), keyframe(0.1, 1), keyframe(0.8, 0));
	add_keyframes(enemy, &enemy->anim[10], 7,
		keyframe(0.2, 0), keyframe(0.2, 1), keyframe(0.2, 2),
		keyframe(0.2, 3), keyframe(0.2, 4), keyframe(0.2, 5),
		keyframe(10, 6));
	add_keyframes(enemy, &enemy->anim[11], 9,
		keyframe(0.1, 0), keyframe(0.1, 1), keyframe(0.1, 2),
		keyframe(0.1, 3), keyframe(0.1, 4), keyframe(0.1, 5),
		keyframe(0.1, 6), keyframe(0.1, 7), keyframe(10, 8));
}

void	add_enemy(int x, int z, t_map *map)
{
	t_enemy	*enemy;

	enemy = malloc(sizeof(t_enemy));
	if (!enemy)
	{
		print_err(MALLOC_ERR);
		// faut free des trucs ici
		exit(1);
	}
	enemy->visible = 0;
	enemy->shoot_distance = 5 + ((float)rand() / RAND_MAX) * 10;
	enemy->pos.x = (x + 0.5) * TILE;
	enemy->pos.y = 0;
	enemy->pos.z = -(z + 0.5) * TILE;
	enemy->mesh = NULL;
	enemy->reverse_uv = 0;
	enemy->x_tile = 9;
	enemy->y_tile = 12;
	enemy->cur_anim = 0;
	enemy->dead = 0;
	add_enemy_keyframes(enemy);
	ft_dl_add_back(&map->enemies, ft_dl_new(enemy));
}

int	check_map_line(char *str, int z, t_map *map)
{
	int	x;

	x = -1;
	while (str[++x])
	{
		if (str[x] == 'N' || str[x] == 'S' || str[x] == 'E' || str[x] == 'W')
		{
			if (map->start_x != -2)
				return (0);
			else
			{
				map->start_x = x;
				map->start_z = z;
				set_start_dir(map, str[x]);
			}
		}
		else if (str[x] == 'B')
			add_enemy(x, z, map);
		else if (!(str[x] == '1' || str[x] == '0' || str[x] == 'P'
				|| str[x] == 'O' || str[x] == 'I' || str[x] == 'U'
				|| str[x] == ' '))
			return (0);
	}
	return (1);
}

int	check_map_char(t_map *map)
{
	t_dlist		*tmp;
	t_map_line	*data;
	int			z;

	z = 0;
	tmp = map->lines;
	while (tmp)
	{
		data = tmp->data;
		data->z = z;
		if (!check_map_line(data->str, z, map))
			return (0);
		tmp = tmp->next;
		z++;
	}
	return (1);
}

int	check_map_content(t_env *env)
{
	env->map.start_x = -2;
	env->map.enemies = NULL;
	if (!check_map_char(&env->map))
		return (print_err(MAP_CHAR));
	if (env->map.start_x == -2)
		return (print_err(MAP_NO_START));
	if (!check_closed_map(&env->map, env->map.start_x, env->map.start_z))
		return (print_err(MAP_NOT_CLOSED));
	return (0);
}
