/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 15:49:22 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 12:42:01 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

float	deg2rad(float angle)
{
	float	rad;

	rad = angle * M_PI / 180;
	return (rad);
}

long	get_timestamp(void)
{
	struct timeval	t;

	gettimeofday(&t, NULL);
	return (t.tv_sec * 1000000 + t.tv_usec);
}

int	get_fps(long t1, long t2)
{
	return (1.0 / ((t2 - t1) / 1000000.0));
}

size_t	ft_strlen_no_space(char *str)
{
	char	*clean_str;
	size_t	len;

	clean_str = ft_strtrim(str, " \f\n\r\t\v");
	len = ft_strlen(clean_str);
	free(clean_str);
	return (len);
}

void	free_split(char **splitted)
{
	int	i;

	i = -1;
	while (splitted[++i])
		free(splitted[i]);
	free(splitted);
}
