/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyframes.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/23 08:41:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 09:40:09 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_keyframe	keyframe(float time, int value)
{
	t_keyframe	key;

	key.time = time * 1000000;
	key.value = value;
	return (key);
}

void	add_keyframes(t_enemy *enemy, t_anim *anim, int nb, ...)
{
	va_list		ap;
	int			i;

	if (!enemy)
		return ;
	va_start(ap, nb);
	i = -1;
	while (++i < nb)
		anim->keyframes[i] = va_arg(ap, t_keyframe);
	anim->nb_keyframes = nb;
	anim->cur_key = 0;
	anim->start = get_timestamp();
	va_end(ap);
}
