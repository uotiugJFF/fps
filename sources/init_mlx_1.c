/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mlx_1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 10:52:06 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 18:10:20 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_mlx_values(t_env *env)
{
	env->mlx = mlx_init();
	if (!env->mlx)
	{
		print_err(MLX_INIT_ERR);
		// some stuff to free here
		exit(1);
	}
	env->win = mlx_new_window(env->mlx, WIN_W, WIN_H, "FPS");
	if (!env->win)
	{
		print_err(MLX_WIN_ERR);
		// some stuff to free here
		exit(1);
	}
	env->img.img = mlx_new_image(env->mlx, WIN_W, WIN_H);
	if (!env->img.img)
	{
		print_err(MLX_IMG_ERR);
		// some stuff to free here
		exit(1);
	}
	env->img.addr = mlx_get_data_addr(env->img.img, &env->img.bpp,
			&env->img.line_len, &env->img.endian);
	env->map.visible_mesh = NULL;
}

void	init_player(t_env *env)
{
	env->player.pos.x = (env->map.start_x + 0.5) * TILE;
	env->player.pos.y = HEIGHT / 2.0;
	env->player.pos.z = -(env->map.start_z + 0.5) * TILE;
	env->player.shoot = 0;
	env->player.shoot_time = 0;
	env->player.pv = 100;
	env->player.pv_max = 100;
	env->player.invincible = 0;
	env->player.invincible_time = 500000;
	env->player.invincible_until = 0;
	env->player.keys.w = 0;
	env->player.keys.a = 0;
	env->player.keys.s = 0;
	env->player.keys.d = 0;
	env->player.keys.mouse = vec3(0, 0, 0);
}

void	init_texture(t_texture *tex, t_env *env)
{
	tex->img.img = mlx_xpm_file_to_image(env->mlx, tex->path,
			&tex->width, &tex->height);
	if (!tex->img.img)
	{
		print_err(MLX_IMG_ERR);
		// some stuff to free here
		exit(1);
	}
	tex->img.addr = mlx_get_data_addr(tex->img.img, &tex->img.bpp,
			&tex->img.line_len, &tex->img.endian);
}

void	init_textures(t_env *env)
{
	init_texture(env->tex_set.north, env);
	init_texture(env->tex_set.south, env);
	init_texture(env->tex_set.east, env);
	init_texture(env->tex_set.west, env);
	init_texture(env->tex_set.floor, env);
	init_texture(env->tex_set.ceiling, env);
	init_texture(env->tex_set.map_n, env);
	init_texture(env->tex_set.enemy, env);
	init_texture(env->tex_set.gun, env);
}

void	init_mlx(t_env *env)
{
	int	i;

	init_mlx_values(env);
	init_player(env);
	env->tex_set.map_n = save_texture_path(ft_strdup("./resources/N.xpm"));
	env->tex_set.enemy = save_texture_path(ft_strdup("./resources/marine.xpm"));
	env->tex_set.gun = save_texture_path(ft_strdup("./resources/gun.xpm"));
	init_textures(env);
	env->delta_time = 0;
	env->debug.infos = 0;
	env->debug.dda = 0;
	env->debug.z = 0;
	env->debug.uv = 0;
	env->debug.light = 0;
	env->debug.wire = 0;
	env->debug.threads = 1;
	env->threads.clip_threads = 100;
	env->render_map = 0;
	env->map.clipped_mesh = NULL;
	mlx_mouse_hide(env->mlx, env->win);
	i = -1;
	while (++i < WIN_W)
		pthread_mutex_init(&env->pxl_mutex[i], NULL);
	init_hooks(env);
}
