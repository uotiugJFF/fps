/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh_wall.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 10:26:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 01:07:11 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	create_tri_wall_1(t_tile *tile, t_vec3 pts[2], t_texture *tex)
{
	t_triangle	*tri_1;

	tri_1 = malloc(sizeof(t_triangle));
	if (!tri_1)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	tri_1->p[0] = vec3(pts[0].x, 0, pts[0].z);
	tri_1->p[1] = vec3(pts[1].x, HEIGHT, pts[1].z);
	tri_1->p[2] = vec3(pts[1].x, 0, pts[1].z);
	tri_1->t[0] = vec2(0, 1);
	tri_1->t[1] = vec2(1, 0);
	tri_1->t[2] = vec2(1, 1);
	tri_1->tex = tex;
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri_1));
}

void	create_tri_wall_2(t_tile *tile, t_vec3 pts[2], t_texture *tex)
{
	t_triangle	*tri_2;

	tri_2 = malloc(sizeof(t_triangle));
	if (!tri_2)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	tri_2->p[0] = vec3(pts[0].x, 0, pts[0].z);
	tri_2->p[1] = vec3(pts[0].x, HEIGHT, pts[0].z);
	tri_2->p[2] = vec3(pts[1].x, HEIGHT, pts[1].z);
	tri_2->t[0] = vec2(0, 1);
	tri_2->t[1] = vec2(0, 0);
	tri_2->t[2] = vec2(1, 0);
	tri_2->tex = tex;
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri_2));
}

void	add_wall(t_tile *tile, t_vec3 pts[2], t_texture *tex)
{
	create_tri_wall_1(tile, pts, tex);
	create_tri_wall_2(tile, pts, tex);
}
