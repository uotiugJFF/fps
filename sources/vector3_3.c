/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector3_3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:31:39 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:31:52 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_vec3	vec3(float x, float y, float z)
{
	t_vec3	vec;

	vec.x = x;
	vec.y = y;
	vec.z = z;
	vec.w = 1;
	return (vec);
}

t_vec3	mat_mult_vec3(t_mat4x4 m, t_vec3 i)
{
	t_vec3	v;

	v.x = i.x * m.m[0][0] + i.y * m.m[1][0] + i.z * m.m[2][0] + i.w * m.m[3][0];
	v.y = i.x * m.m[0][1] + i.y * m.m[1][1] + i.z * m.m[2][1] + i.w * m.m[3][1];
	v.z = i.x * m.m[0][2] + i.y * m.m[1][2] + i.z * m.m[2][2] + i.w * m.m[3][2];
	v.w = i.x * m.m[0][3] + i.y * m.m[1][3] + i.z * m.m[2][3] + i.w * m.m[3][3];
	return (v);
}

void	tri_mat_mult_vec3(t_triangle i, t_triangle *o, t_mat4x4 m)
{
	o->p[0] = mat_mult_vec3(m, i.p[0]);
	o->p[1] = mat_mult_vec3(m, i.p[1]);
	o->p[2] = mat_mult_vec3(m, i.p[2]);
}

void	ft_vec3swap(t_vec3 *a, t_vec3 *b)
{
	t_vec3	tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

t_vec3	get_normal(t_triangle *tr)
{
	t_vec3	normal;
	t_vec3	s1;
	t_vec3	s2;

	s1 = sub_vec3(tr->p[1], tr->p[0]);
	s2 = sub_vec3(tr->p[2], tr->p[0]);
	normal = cross_product(s1, s2);
	normalize_vec3(&normal);
	return (normal);
}
