/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_star_1.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 08:41:13 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 11:49:43 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_a_star(t_a_star_infos *i, t_vec2 start, t_vec2 end, t_enemy *enemy)
{
	i->to_test = NULL;
	i->current = NULL;
	i->node = NULL;
	i->start.x = start.u;
	i->start.y = start.v;
	i->end.x = end.u;
	i->end.y = end.v;
	i->enemy = enemy;
}

void	reset_path(t_a_star_infos *i, t_env *env)
{
	int	x;
	int	z;

	z = -1;
	while (++z < env->map.z_max)
	{
		x = -1;
		while (++x < env->map.x_max)
		{
			init_path(&env->map.path[z][x], x, z, env);
		}
	}
	env->map.path[i->start.y][i->start.x].local_goal = 0;
	env->map.path[i->start.y][i->start.x].global_goal
		= heuristic_distance(vec2(i->start.x, i->start.y),
			vec2(i->end.x, i->end.y));
}

void	clean_a_star_list(t_a_star_infos *i)
{
	t_dlist	*tmp;

	tmp = i->to_test;
	while (tmp)
	{
		i->node = tmp->data;
		if (i->node->visited)
			tmp = ft_dl_remove_from(&i->to_test, tmp, &del_path_node);
		else
			tmp = tmp->next;
	}
}

void	check_a_star_neighboors(t_a_star_infos *i, t_env *env)
{
	if (i->node->x + 1 < env->map.x_max)
		check_neighboor(i, vec2(i->node->x + 1, i->node->z), env);
	if (i->node->x - 1 > 0)
		check_neighboor(i, vec2(i->node->x - 1, i->node->z), env);
	if (i->node->z + 1 < env->map.z_max)
		check_neighboor(i, vec2(i->node->x, i->node->z + 1), env);
	if (i->node->z - 1 > 0)
		check_neighboor(i, vec2(i->node->x, i->node->z - 1), env);
}

void	set_walk_anim(t_enemy *enemy, int anim)
{
	if (enemy->cur_anim != anim)
	{
		enemy->cur_anim = anim;
		enemy->anim[anim].cur_key = 0;
		enemy->anim[anim].start = get_timestamp();
	}
}

int	player_visible(t_enemy *enemy, t_env *env)
{
	t_vec3	dir;
	t_dda	dda;

	dda.start_pos.u = enemy->pos.x / TILE;
	dda.start_pos.v = -enemy->pos.z / TILE + 1;
	dir = sub_vec3(env->player.pos, enemy->pos);
	dir.y = 0;
	normalize_vec3(&dir);
	dda.look_dir.u = dir.x;
	dda.look_dir.v = -dir.z;
	dda.v_unit_step.u = fabs(1.0 / dda.look_dir.u);
	dda.v_unit_step.v = fabs(1.0 / dda.look_dir.v);
	dda.map_check.u = (int)dda.start_pos.u;
	dda.map_check.v = (int)dda.start_pos.v;
	if (dda.look_dir.u < 0)
	{
		dda.v_step.u = -1;
		dda.v_length.u = (dda.start_pos.u - dda.map_check.u)
			* dda.v_unit_step.u;
	}
	else
	{
		dda.v_step.u = 1;
		dda.v_length.u = ((dda.map_check.u + 1) - dda.start_pos.u)
			* dda.v_unit_step.u;
	}
	if (dda.look_dir.v < 0)
	{
		dda.v_step.v = -1;
		dda.v_length.v = (dda.start_pos.v - dda.map_check.v)
			* dda.v_unit_step.v;
	}
	else
	{
		dda.v_step.v = 1;
		dda.v_length.v = ((dda.map_check.v + 1) - dda.start_pos.v)
			* dda.v_unit_step.v;
	}
	dda.dist = 0;
	dda.max_dist = 100;
	while (dda.dist < dda.max_dist)
	{
		if (dda.v_length.u < dda.v_length.v)
		{
			dda.map_check.u += dda.v_step.u;
			dda.dist = dda.v_length.u;
			dda.v_length.u += dda.v_unit_step.u;
		}
		else
		{
			dda.map_check.v += dda.v_step.v;
			dda.dist = dda.v_length.v;
			dda.v_length.v += dda.v_unit_step.v;
		}
		if (dda.map_check.u >= 0 && dda.map_check.u < env->map.x_max
			&& dda.map_check.v - 1 > 0 && dda.map_check.v - 1 <= env->map.z_max)
		{
			if(env->map.map[(int)dda.map_check.v - 1][(int)dda.map_check.u].type == WALL)
			{
				enemy->visible = 0;
				return (0);
			}
			if((int)(env->player.pos.x / TILE) == dda.map_check.u
					&& (int)(-env->player.pos.z / TILE) == dda.map_check.v - 1)
			{
				enemy->visible = 1;
				return (1);
			}
		}

	}
	return (0);
}

void	enemy_ai(t_enemy *enemy, t_path_node *move_node, t_env *env)
{
	enemy->look_dir = sub_vec3(
			vec3((move_node->x + 0.5) * TILE, 0,
				-(move_node->z + 0.5) * TILE), enemy->pos);
	if (enemy->dead)
		return ;
	player_visible(enemy, env);
	if (len_vec3(sub_vec3(enemy->pos, env->player.pos)) > enemy->shoot_distance
			|| !player_visible(enemy, env))
	{
		enemy->shoot_distance = 5 + ((float)rand() / RAND_MAX) * 10;
		t_vec3	p_look_dir;
		p_look_dir = get_look_dir(env);
		normalize_vec3(&enemy->look_dir);
		int	angle;
		angle = rad2deg(acosf(dot_product(p_look_dir, enemy->look_dir)));
		float	dir;
		dir = cross_product(p_look_dir, enemy->look_dir).y;
		if (angle > 157)
			set_walk_anim(enemy, 0);
		else if (angle > 112)
			set_walk_anim(enemy, 2);
		else if (angle > 67)
			set_walk_anim(enemy, 4);
		else if (angle > 22)
			set_walk_anim(enemy, 6);
		else
			set_walk_anim(enemy, 8);
		if (dir < 0)
			enemy->reverse_uv = 0;
		else
			enemy->reverse_uv = 1;
		if (env->delta_time != 0)
			enemy->pos = add_vec3(enemy->pos,
					mult_vec3(enemy->look_dir, env->delta_time / 300000.0));
	}
	else
	{
		if (enemy->cur_anim != 1)
		{
			enemy->reverse_uv = 0;
			enemy->cur_anim = 1;
			enemy->anim[1].cur_key = 0;
			enemy->anim[1].start = get_timestamp();
		}
	}
}

void	a_star(t_vec2 start, t_vec2 end, t_enemy *enemy, t_env *env)
{
	t_a_star_infos	i;
	t_path_node		*end_node;
	t_path_node		*move_node;

	init_a_star(&i, start, end, enemy);
	reset_path(&i, env);
	i.current = ft_dl_new(&env->map.path[i.start.y][i.start.x]);
	ft_dl_add_back(&i.to_test, i.current);
	while (i.to_test && i.current->data != &env->map.path[i.end.y][i.end.x])
	{
		ft_dl_sort(i.to_test, &sort_node);
		clean_a_star_list(&i);
		if (!i.to_test)
			break ;
		i.current = i.to_test;
		i.node = i.current->data;
		i.node->visited = 1;
		check_a_star_neighboors(&i, env);
	}
	move_node = NULL;
	end_node = &env->map.path[i.end.y][i.end.x];
	while (end_node->parent)
	{
		if (env->debug.dda)
			draw_a_star_path(end_node->x, end_node->z, 0xff5500, env);
		move_node = end_node;
		end_node = end_node->parent;
	}
	if (move_node)
		enemy_ai(enemy, move_node, env);
}
