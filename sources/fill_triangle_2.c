/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_triangle_2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:28:46 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:29:03 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	sort_points(t_triangle *tr)
{
	if (tr->p[0].y > tr->p[1].y)
	{
		ft_vec3swap(&tr->p[0], &tr->p[1]);
		ft_vec2swap(&tr->t[0], &tr->t[1]);
	}
	if (tr->p[1].y > tr->p[2].y)
	{
		ft_vec3swap(&tr->p[1], &tr->p[2]);
		ft_vec2swap(&tr->t[1], &tr->t[2]);
		if (tr->p[0].y > tr->p[1].y)
		{
			ft_vec3swap(&tr->p[0], &tr->p[1]);
			ft_vec2swap(&tr->t[0], &tr->t[1]);
		}
	}
}
