/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda_calculation.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 11:33:10 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 23:29:31 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_dda(t_dda *dda, float shift, t_env *env)
{
	t_vec3		target;
	t_mat4x4	mat_cam_rot;
	t_vec3		right_3d;
	t_vec2		right;
	t_dlist		*tmp;

	target = vec3(0, 0, 1);
	dda->angle = env->map.dir + 90 + env->player.keys.mouse.x / -5.0;
	mat_cam_rot = mat_rotate_y(dda->angle);
	right_3d = mat_mult_vec3(mat_cam_rot, target);
	right.u = right_3d.x;
	right.v = -right_3d.z;
	dda->start_pos.u = env->player.pos.x / TILE + right.u * shift;
	dda->start_pos.v = -env->player.pos.z / TILE + 1 + right.v * shift;
	dda->angle = env->map.dir + env->player.keys.mouse.x / -5.0;
	env->map.map[(int)dda->start_pos.v - 1][(int)dda->start_pos.u].visible = 1;
	tmp = ft_dl_new(
			&env->map.map[(int)dda->start_pos.v - 1][(int)dda->start_pos.u]);
	ft_dl_add_back(&env->map.visible_mesh, tmp);
}

void	init_dda_length(t_dda *dda)
{
	if (dda->look_dir.u < 0)
	{
		dda->v_step.u = -1;
		dda->v_length.u = (dda->start_pos.u - dda->map_check.u)
			* dda->v_unit_step.u;
	}
	else
	{
		dda->v_step.u = 1;
		dda->v_length.u = ((dda->map_check.u + 1) - dda->start_pos.u)
			* dda->v_unit_step.u;
	}
	if (dda->look_dir.v < 0)
	{
		dda->v_step.v = -1;
		dda->v_length.v = (dda->start_pos.v - dda->map_check.v)
			* dda->v_unit_step.v;
	}
	else
	{
		dda->v_step.v = 1;
		dda->v_length.v = ((dda->map_check.v + 1) - dda->start_pos.v)
			* dda->v_unit_step.v;
	}
}

void	init_dda_ray(t_dda *dda, float angle)
{
	t_mat4x4	mat_cam_rot;
	t_vec3		target;
	t_vec3		look_dir_3d;

	target = vec3(0, 0, 1);
	mat_cam_rot = mat_rotate_y(dda->angle + angle);
	look_dir_3d = mat_mult_vec3(mat_cam_rot, target);
	dda->look_dir.u = look_dir_3d.x;
	dda->look_dir.v = -look_dir_3d.z;
	dda->v_unit_step.u = fabs(1.0 / dda->look_dir.u);
	dda->v_unit_step.v = fabs(1.0 / dda->look_dir.v);
	dda->map_check.u = (int)dda->start_pos.u;
	dda->map_check.v = (int)dda->start_pos.v;
	init_dda_length(dda);
}

void	add_other(t_tile *tile, t_env *env)
{
	t_dlist	*tmp;

	if (!tile->visible)
	{
		tile->visible = 1;
		if (tile->type == DOOR)
		{
			//memory leaks
			tile->mesh = NULL;
			build_doors(tile, env);
			tmp = ft_dl_new(tile);
			ft_dl_add_back(&env->map.visible_mesh, tmp);
		}
		else
		{
			tmp = ft_dl_new(tile);
			ft_dl_add_back(&env->map.visible_mesh, tmp);
		}
	}
}

void	dda_check_ray(t_dda *dda, t_env *env)
{
	t_dlist	*tmp;
	int		x;
	int		z;

	x = (int)(dda->map_check.u);
	z = (int)(dda->map_check.v - 1);
	if (env->map.map[z][x].type == WALL)
	{
		dda->wall_found = 1;
		if (!env->map.map[z][x].visible)
		{
			env->map.map[z][x].visible = 1;
			tmp = ft_dl_new(&env->map.map[z][x]);
			ft_dl_add_back(&env->map.visible_mesh, tmp);
		}
	}
	else
	{
		if (env->map.map[z][x].type == DOOR
				&& env->map.map[z][x].door_status == 0)
			dda->wall_found = 1;
		add_other(&env->map.map[z][x], env);
	}
}
