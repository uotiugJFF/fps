/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tex_triangle_3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 02:36:13 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 13:52:30 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	clr_from_uv(t_vec3 uvw)
{
	t_rgb_clr	rgb;

	rgb = rgb_from_int(0);
	rgb.r = 255 * minf(uvw.x, 1);
	rgb.g = 255 * minf(uvw.y, 1);
	rgb.b = 0;
	return (int_from_rgb(rgb));
}

int	clr_from_z(t_vec3 uvw)
{
	t_rgb_clr	rgb;

	rgb = rgb_from_int(0);
	rgb.r = 255 * minf(uvw.z * 2, 1);
	rgb.g = 255 * minf(uvw.z * 2, 1);
	rgb.b = 255 * minf(uvw.z * 2, 1);
	return (int_from_rgb(rgb));
}

int	clr_from_tex(t_triangle tr, t_vec3 uvw)
{
	int			x;
	int			y;
	int			clr;
	int			transparency;
	t_rgb_clr	rgb;

	x = maxf(minf(fabs(uvw.x), 1), 0) * tr.tex->width;
	y = maxf(minf(fabs(uvw.y), 1), 0) * tr.tex->height;
	clr = (*(int *)(tr.tex->img.addr + (4 * tr.tex->width * y) + (4 * x)));
	rgb = rgb_from_int(clr);
	rgb.r = rgb.r * minf(tr.light * 2, 1) * minf(uvw.z * 7, 1);
	rgb.g = rgb.g * minf(tr.light * 2, 1) * minf(uvw.z * 7, 1);
	rgb.b = rgb.b * minf(tr.light * 2, 1) * minf(uvw.z * 7, 1);
	transparency = clr >> 24;
	transparency <<= 24;
	clr = int_from_rgb(rgb) | transparency;
	return (clr);
}
