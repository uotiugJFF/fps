/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_enemies.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 10:17:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 12:08:00 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

float	calc_enemy_rotation(t_enemy *enemy, t_env *env)
{
	t_vec3	norm;
	float	angle;

	norm = sub_vec3(enemy->pos, env->player.pos);
	norm.y = 0;
	normalize_vec3(&norm);
	angle = acos(dot_product(vec3(0, 0, 1), norm));
	if (norm.x < 0)
		angle *= -1;
	return (rad2deg(angle));
}

float	get_key_value(t_enemy *enemy)
{
	t_anim	*anim;
	long	time;

	anim = &enemy->anim[enemy->cur_anim];
	time = get_timestamp();
	if (anim->start + anim->keyframes[anim->cur_key].time < time)
	{
		anim->cur_key = (anim->cur_key + 1) % anim->nb_keyframes;
		anim->start = time;
	}
	return (anim->keyframes[anim->cur_key].value);
}

void	del_enemies_list(void *data)
{
	// need to del enemy correctly;
	(void) data;
}

void	create_enemy_tri_1(t_enemy *enemy, t_vec3 s, t_vec3 e, t_env *env)
{
	t_triangle	*tri_1;
	float		key_value;

	key_value = get_key_value(enemy);
	tri_1 = malloc(sizeof(t_triangle));
	if (!tri_1)
	{
		print_err(MALLOC_ERR);
		// some stuff to do
		exit(MALLOC_ERR);
	}
	tri_1->p[0] = vec3(s.x, 0, s.z);
	tri_1->p[1] = vec3(e.x, 4, e.z);
	tri_1->p[2] = vec3(e.x, 0, e.z);
	tri_1->t[0] = vec2(key_value / enemy->x_tile,
			(float)(enemy->cur_anim + 1) / enemy->y_tile);
	tri_1->t[1] = vec2((key_value + 1) / enemy->x_tile,
			(float)enemy->cur_anim / enemy->y_tile);
	tri_1->t[2] = vec2((key_value + 1) / enemy->x_tile,
			(float)(enemy->cur_anim + 1) / enemy->y_tile);
	if (enemy->reverse_uv)
	{
		tri_1->t[0] = vec2((key_value + 1) / enemy->x_tile,
				(float)(enemy->cur_anim + 1) / enemy->y_tile);
		tri_1->t[1] = vec2(key_value / enemy->x_tile,
				(float)enemy->cur_anim / enemy->y_tile);
		tri_1->t[2] = vec2(key_value / enemy->x_tile,
				(float)(enemy->cur_anim + 1) / enemy->y_tile);
	}
	tri_1->tex = env->tex_set.enemy;
	ft_dl_add_back(&enemy->mesh, ft_dl_new(tri_1));
}

void	create_enemy_tri_2(t_enemy *enemy, t_vec3 s, t_vec3 e, t_env *env)
{
	t_triangle	*tri_2;
	float		key_value;

	key_value = get_key_value(enemy);
	tri_2 = malloc(sizeof(t_triangle));
	if (!tri_2)
	{
		print_err(MALLOC_ERR);
		// some stuff to do
		exit(MALLOC_ERR);
	}
	tri_2->p[0] = vec3(s.x, 0, s.z);
	tri_2->p[1] = vec3(s.x, 4, s.z);
	tri_2->p[2] = vec3(e.x, 4, e.z);
	tri_2->t[0] = vec2(key_value / enemy->x_tile,
			(float)(enemy->cur_anim + 1) / enemy->y_tile);
	tri_2->t[1] = vec2(key_value / enemy->x_tile,
			(float)enemy->cur_anim / enemy->y_tile);
	tri_2->t[2] = vec2((key_value + 1) / enemy->x_tile,
			(float)enemy->cur_anim / enemy->y_tile);
	tri_2->tex = env->tex_set.enemy;
	if (enemy->reverse_uv)
	{
		tri_2->t[0] = vec2((key_value + 1) / enemy->x_tile,	
				(float)(enemy->cur_anim + 1) / enemy->y_tile);
		tri_2->t[1] = vec2((key_value + 1) / enemy->x_tile,
				(float)enemy->cur_anim / enemy->y_tile);
		tri_2->t[2] = vec2(key_value / enemy->x_tile,
				(float)enemy->cur_anim / enemy->y_tile);
	}
	ft_dl_add_back(&enemy->mesh, ft_dl_new(tri_2));
}

void	build_enemies(t_env *env)
{
	t_vec3		start;
	t_vec3		end;
	t_dlist		*enemies_list;
	t_enemy		*enemy;
	float		angle;
	float		key_value;

	enemies_list = env->map.enemies;
	while (enemies_list)
	{
		enemy = enemies_list->data;
		// need to free meshes correctly
		enemy->mesh = NULL;
		start = vec3(-2, 0, 0);
		end = vec3(2, 0, 0);
		angle = calc_enemy_rotation(enemy, env);
		start = rotate_y(start, angle);
		end = rotate_y(end, angle);
		a_star(vec2(enemy->pos.x / TILE, -enemy->pos.z / TILE),
			vec2(env->player.pos.x / TILE, -env->player.pos.z / TILE), enemy,
			env);
		start = add_vec3(enemy->pos, start);
		end = add_vec3(enemy->pos, end);
		key_value = get_key_value(enemy);
		if (enemy->dead && enemy->death_time + 10000000 < get_timestamp())
		{
			ft_dl_remove_from(&env->map.enemies, enemies_list,
				&del_enemies_list);
		}
		if (enemy->cur_anim == 1 && key_value == 1 && !env->player.invincible)
		{
			env->player.invincible = 1;
			env->player.invincible_until = get_timestamp()
				+ env->player.invincible_time;
			env->player.pv = max(0, env->player.pv - 20);
		}
		create_enemy_tri_1(enemy, start, end, env);
		create_enemy_tri_2(enemy, start, end, env);
		enemies_list = enemies_list->next;
	}
}
