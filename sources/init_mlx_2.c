/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_mlx_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:45:08 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/22 16:23:32 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	init_hooks(t_env *env)
{
	mlx_hook(env->win, 17, (1L << 3), close_win, env);
	mlx_hook(env->win, 2, (1L << 0), press_key, env);
	mlx_hook(env->win, 3, (1L << 1), release_key, env);
	mlx_mouse_hook(env->win, mouse_hook, env);
	mlx_loop_hook(env->mlx, &render, env);
	mlx_loop(env->mlx);
}
