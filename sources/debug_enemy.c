/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug_enemy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 08:40:16 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 19:25:13 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	draw_enemy_debug(t_enemy *enemy, t_env *env)
{
	int	x;
	int	y;

	x = -3;
	while (++x < 2)
	{
		y = -2;
		while (++y < 3)
		{
			put_pxl(env, WIN_W / 2.0 - (env->map.x_max * 2 * TILE) / 2.0
				+ enemy->pos.x * 2 + x,
				WIN_H / 2.0 - (env->map.z_max * 2 * TILE) / 2
				+ env->map.z_max * 2 * TILE + enemy->pos.z * 2 + y, 0xff6600);
		}
	}
}

void	debug_enemy(t_env *env)
{
	t_dlist	*enemy_list;
	t_enemy	*enemy;
	int		x;
	int		z;

	enemy_list = env->map.enemies;
	while (enemy_list)
	{
		enemy = enemy_list->data;
		x = enemy->pos.x / TILE;
		z = -enemy->pos.z / TILE;
		draw_enemy_debug(enemy, env);
		a_star(vec2(x, z),
			vec2(env->player.pos.x / TILE, -env->player.pos.z / TILE), enemy,
			env);
		enemy_list = enemy_list->next;
	}
}
