/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_1.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 12:04:54 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 18:20:48 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	calc_pxl_clr(t_env *env, int x, int y, int clr)
{
	float		transparency;
	t_rgb_clr	bg_clr;
	t_rgb_clr	new_clr;

	transparency = (clr >> 24 & 0xFF) / 255.0;
	if (transparency == 0)
		return (clr);
	bg_clr = rgb_from_int(*(int *)(env->img.addr + ((WIN_H - 1 - y)
					* env->img.line_len + x * (env->img.bpp / 8))));
	new_clr = rgb_from_int(clr);
	new_clr.r = (transparency) * bg_clr.r + (1.0 - transparency) * new_clr.r;
	new_clr.g = (transparency) * bg_clr.g + (1.0 - transparency) * new_clr.g;
	new_clr.b = (transparency) * bg_clr.b + (1.0 - transparency) * new_clr.b;
	return (int_from_rgb(new_clr));
}

void	put_pxl(t_env *env, int x, int y, int clr)
{
	char	*pxl;
	int		i;

	if ((clr >> 24 & 0xff) == 255)
		return ;
	clr = calc_pxl_clr(env, x, y, clr);
	if (env->render_map && (x - MAP_POS_X) * (x - MAP_POS_X) + (y - MAP_POS_Y)
		* (y - MAP_POS_Y) > (MAP_RADIUS * MAP_RADIUS))
		return ;
	if (!(x >= 0 && x < WIN_W && y >= 0 && y < WIN_H))
		return ;
	i = env->img.bpp - 8;
	pxl = env->img.addr + ((WIN_H - 1 - y) * env->img.line_len + x
			* (env->img.bpp / 8));
	while (i >= 0)
	{
		if (env->img.endian != 0)
			*pxl++ = (clr >> i) & 0xFF;
		else
			*pxl++ = (clr >> (env->img.bpp - 8 - i)) & 0xFF;
		i -= 8;
	}
}

void	render_bg(t_env *env)
{
	int	x;
	int	y;

	x = -1;
	while (++x < WIN_W)
	{
		y = -1;
		while (++y < WIN_H)
			put_pxl(env, x, y, 0x0);
	}
}

void	render_life_bar(t_env *env)
{
	int	y;

	draw_line(vec3(19, WIN_H - 17, 0),
		vec3(env->player.pv_max * 2 + 23, WIN_H - 17, 0), 0x55FF55, env);
	draw_line(vec3(19, WIN_H - 18, 0),
		vec3(env->player.pv_max * 2 + 23, WIN_H - 18, 0), 0x55FF55, env);
	draw_line(vec3(19, WIN_H - 38, 0),
		vec3(env->player.pv_max * 2 + 23, WIN_H - 38, 0), 0x55FF55, env);
	draw_line(vec3(19, WIN_H - 39, 0),
		vec3(env->player.pv_max * 2 + 23, WIN_H - 39, 0), 0x55FF55, env);
	draw_line(vec3(17, WIN_H - 17, 0),
		vec3(17, WIN_H - 40, 0), 0x55FF55, env);
	draw_line(vec3(18, WIN_H - 17, 0),
		vec3(18, WIN_H - 40, 0), 0x55FF55, env);
	draw_line(vec3(env->player.pv_max * 2 + 23, WIN_H - 17, 0),
		vec3(env->player.pv_max * 2 + 23, WIN_H - 40, 0), 0x55FF55, env);
	draw_line(vec3(env->player.pv_max * 2 + 24, WIN_H - 17, 0),
		vec3(env->player.pv_max * 2 + 24, WIN_H - 40, 0), 0x55FF55, env);
	y = -1;
	while (++y < 15)
		draw_line(vec3(21, WIN_H - 21 - y, 0),
			vec3(env->player.pv * 2 + 21, WIN_H - 21 - y, 0), 0x55FF55, env);
}

void	render_ui(t_env *env)
{
	draw_line(vec3(WIN_W / 2 - 6, WIN_H / 2, 0),
		vec3(WIN_W / 2 + 7, WIN_H / 2, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2, WIN_H / 2 - 6, 0),
		vec3(WIN_W / 2, WIN_H / 2 + 7, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2 - 6, WIN_H / 2 - 1, 0),
		vec3(WIN_W / 2 + 7, WIN_H / 2 - 1, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2 + 1, WIN_H / 2 - 6, 0),
		vec3(WIN_W / 2 + 1, WIN_H / 2 + 7, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2 - 6, WIN_H / 2 + 1, 0),
		vec3(WIN_W / 2 + 7, WIN_H / 2 + 1, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2 - 1, WIN_H / 2 - 6, 0),
		vec3(WIN_W / 2 - 1, WIN_H / 2 + 7, 0), 0x0000aa, env);
	draw_line(vec3(WIN_W / 2 - 5, WIN_H / 2, 0),
		vec3(WIN_W / 2 + 6, WIN_H / 2, 0), 0x55ff55, env);
	draw_line(vec3(WIN_W / 2, WIN_H / 2 - 5, 0),
		vec3(WIN_W / 2, WIN_H / 2 + 6, 0), 0x55ff55, env);
	render_life_bar(env);
}
void	render_gun(t_env *env)
{
	t_triangle	*tri_1;
	float		key_value;

	long		time;

	time = get_timestamp();

	time -= env->player.shoot_time;
	if (time < 100000)
		key_value = 0;
	else if (time < 350000)
		key_value = 1;
	else if (time < 450000)
		key_value = 2;
	else if (time < 700000)
		key_value = 3;
	else
		key_value = 0;
	tri_1 = malloc(sizeof(t_triangle));
	if (!tri_1)
	{
		print_err(MALLOC_ERR);
		// some stuff to do
		exit(MALLOC_ERR);
	}
	tri_1->p[0] = vec3(WIN_W / 2 - 151, 0, 0);
	tri_1->p[1] = vec3(WIN_W / 2 + 151, 340, 0);
	tri_1->p[2] = vec3(WIN_W / 2 + 151, 0, 0);
	tri_1->t[0] = vec2(key_value / 4.0, 1);
	tri_1->t[1] = vec2((key_value + 1) / 4.0, 0);
	tri_1->t[2] = vec2((key_value + 1) / 4.0, 1);
	tri_1->tex = env->tex_set.gun;
	tri_1->light = 1;
	tex_triangle(*tri_1, env);
	tri_1->p[0] = vec3(WIN_W / 2 - 151, 0, 0);
	tri_1->p[1] = vec3(WIN_W / 2 - 151, 340, 0);
	tri_1->p[2] = vec3(WIN_W / 2 + 151, 340, 0);
	tri_1->t[0] = vec2(key_value / 4.0, 1);
	tri_1->t[1] = vec2(key_value / 4.0, 0);
	tri_1->t[2] = vec2((key_value + 1) / 4.0, 0);
	tri_1->tex = env->tex_set.gun;
	tri_1->light = 1;
	tex_triangle(*tri_1, env);
	// need to free this triangle.
}

void	screen_render(t_env *env)
{
	render_bg(env);
	render_mesh(env);
	render_map_bg(env);
	render_gun(env);
	env->render_map = 1;
	render_map(env);
	env->render_map = 0;
	render_map_n(env);
	render_ui(env);
	if (env->debug.dda)
		debug_dda(env);
}

int	render(t_env *env)
{
	long	t1;
	long	t2;
	int		x_init;
	int		y_init;

	t1 = get_timestamp();
	if (env->player.invincible && env->player.invincible_until < t1)
		env->player.invincible = 0;
	reset_z_buffer(env);
	mlx_mouse_get_pos(env->mlx, env->win, &x_init, &y_init);
	ft_dl_clear(&env->map.visible_mesh, &reset_tile);
	list_visible_mesh(env);
	move_player(env);
	screen_render(env);
	set_mouse_pos(env, x_init, y_init);
	mlx_put_image_to_window(env->mlx, env->win, env->img.img, 0, 0);
	t2 = get_timestamp();
	env->delta_time = t2 - t1;
	debug_infos(t1, t2, env);
	return (0);
}
