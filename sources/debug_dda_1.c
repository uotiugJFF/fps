/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug_dda_1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:14:27 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 09:03:21 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	debug_bg_map(t_env	*env)
{
	int	x;
	int	z;
	int	start_x;
	int	start_z;

	start_x = WIN_W / 2.0 - (env->map.x_max * 2 * TILE) / 2;
	start_z = WIN_H / 2.0 - (env->map.z_max * 2 * TILE) / 2;
	z = start_z - 1 - 20;
	while (++z < start_z + 20 + env->map.z_max * 2 * TILE)
	{
		x = start_x - 1 - 20;
		while (++x < start_x + 20 + env->map.x_max * 2 * TILE)
		{
			put_pxl(env, x, z, 0x77000000);
		}
	}
}

void	debug_map(t_env *env)
{
	int		x;
	int		z;

	debug_bg_map(env);
	z = -1;
	while (++z < env->map.z_max)
	{
		x = -1;
		while (++x < env->map.x_max)
		{
			if (env->map.map[z][x].type == WALL)
				debug_tile(x, z, 0xa2a2a2, env);
			else if (env->map.map[z][x].type == DOOR)
			{
				if (env->map.map[z][x].door_status == 2)
					debug_tile(x, z, 0xa2a2ff, env);
				else
					debug_tile(x, z, 0xffa2a2, env);
			}
		}
	}
}

void	debug_dda_fov(float angle, int clr, t_env *env)
{
	t_mat4x4	mat_cam_rot;
	t_vec2		start_pos;
	t_vec3		target;
	t_vec3		look_dir_3d;
	t_vec2		look_dir;

	start_pos.u = env->player.pos.x / TILE;
	start_pos.v = -env->player.pos.z / TILE;
	target = vec3(0, 0, 1);
	angle += -env->map.dir + env->player.keys.mouse.x / 5.0;
	mat_cam_rot = mat_rotate_y(angle);
	look_dir_3d = mat_mult_vec3(mat_cam_rot, target);
	look_dir.u = look_dir_3d.x;
	look_dir.v = -look_dir_3d.z;
	draw_line(vec3(WIN_W / 2.0 - (env->map.x_max * 2 * TILE) / 2
			+ start_pos.u * 2 * TILE,
			WIN_H / 2.0 - (env->map.z_max * 2 * TILE) / 2
			+ env->map.z_max * 2 * TILE - start_pos.v * 2 * TILE, 0),
		vec3(WIN_W / 2.0 - (env->map.x_max * 2 * TILE) / 2
			+ start_pos.u * 2 * TILE - look_dir.u * 200,
			WIN_H / 2.0 - (env->map.z_max * 2 * TILE) / 2
			+ env->map.z_max * 2 * TILE - start_pos.v * 2 * TILE
			- look_dir.v * 200, 0),
		clr, env);
}

void	debug_dda(t_env *env)
{
	t_dlist	*visible_mesh;
	int		x;
	int		z;

	debug_map(env);
	visible_mesh = env->map.visible_mesh;
	while (visible_mesh)
	{
		x = ((t_tile *)visible_mesh->data)->x;
		z = ((t_tile *)visible_mesh->data)->z;
		if (((t_tile *)visible_mesh->data)->type == WALL)
			debug_tile(x, z, 0x00cc00, env);
		else
			debug_tile(x, z, 0xbbffbb, env);
		visible_mesh = visible_mesh->next;
	}
	debug_dda_fov(FOV / 2.0 * WIN_W / WIN_H, 0x2299ff, env);
	debug_dda_fov(-FOV / 2.0 * WIN_W / WIN_H, 0x2299ff, env);
	debug_dda_fov(FOV / 2.0 * WIN_W / WIN_H + 10, 0xff99ff, env);
	debug_dda_fov(-FOV / 2.0 * WIN_W / WIN_H - 10, 0xff99ff, env);
	debug_enemy(env);
}
