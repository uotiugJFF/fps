/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_mesh_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:34:45 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:44:13 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	to_cartesian_space(t_triangle *tr)
{
	tr->p[0] = div_vec3(tr->p[0], tr->p[0].w);
	tr->p[1] = div_vec3(tr->p[1], tr->p[1].w);
	tr->p[2] = div_vec3(tr->p[2], tr->p[2].w);
	tr->t[0].u = tr->t[0].u / tr->p[0].w;
	tr->t[1].u = tr->t[1].u / tr->p[1].w;
	tr->t[2].u = tr->t[2].u / tr->p[2].w;
	tr->t[0].v = tr->t[0].v / tr->p[0].w;
	tr->t[1].v = tr->t[1].v / tr->p[1].w;
	tr->t[2].v = tr->t[2].v / tr->p[2].w;
	tr->t[0].w = 1.0 / tr->p[0].w;
	tr->t[1].w = 1.0 / tr->p[1].w;
	tr->t[2].w = 1.0 / tr->p[2].w;
}

void	to_screen(t_triangle *tr)
{
	t_vec3	offset;

	offset = vec3(1, 1, 0);
	tr->p[0] = add_vec3(tr->p[0], offset);
	tr->p[1] = add_vec3(tr->p[1], offset);
	tr->p[2] = add_vec3(tr->p[2], offset);
	tr->p[0].x *= 0.5 * (float)WIN_W;
	tr->p[0].y *= 0.5 * (float)WIN_H;
	tr->p[1].x *= 0.5 * (float)WIN_W;
	tr->p[1].y *= 0.5 * (float)WIN_H;
	tr->p[2].x *= 0.5 * (float)WIN_W;
	tr->p[2].y *= 0.5 * (float)WIN_H;
}
