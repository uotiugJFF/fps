/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug_dda_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 13:23:03 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 19:05:16 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	debug_tile(int x, int y, int clr, t_env *env)
{
	int	i;
	int	j;
	int	x_start;
	int	y_start;

	x_start = WIN_W / 2.0 - (env->map.x_max * 2 * TILE) / 2;
	y_start = WIN_H / 2.0 - (env->map.z_max * 2 * TILE) / 2;
	j = 0;
	while (++j < 2 * TILE - 1)
	{
		i = 0;
		while (++i < 2 * TILE - 1)
			put_pxl(env, x_start + x * TILE * 2 + i,
				y_start + (env->map.z_max * TILE * 2) - (y * TILE * 2 + j),
				clr);
	}
}
