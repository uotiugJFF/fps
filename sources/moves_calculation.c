/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves_calculation.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 15:35:23 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 13:39:38 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	move_forward(t_env *env)
{
	t_vec3		next_pos;
	t_mat4x4	mat_cam_rot;
	t_vec3		target;
	t_vec3		look_dir;

	target = vec3(0, 0, 1);
	mat_cam_rot = mat_rotate_y(env->map.dir + env->player.keys.mouse.x / -5.0);
	look_dir = mat_mult_vec3(mat_cam_rot, target);
	next_pos = add_vec3(env->player.pos,
			mult_vec3(look_dir, (float)env->delta_time / 100000.0));
	do_player_move(env, next_pos);
}

void	move_backward(t_env *env)
{
	t_vec3		next_pos;
	t_mat4x4	mat_cam_rot;
	t_vec3		target;
	t_vec3		look_dir;

	target = vec3(0, 0, 1);
	mat_cam_rot = mat_rotate_y(env->map.dir + env->player.keys.mouse.x / -5.0);
	look_dir = mat_mult_vec3(mat_cam_rot, target);
	next_pos = sub_vec3(env->player.pos,
			mult_vec3(look_dir, (float)env->delta_time / 100000.0));
	do_player_move(env, next_pos);
}

void	move_left(t_env *env)
{
	t_vec3		next_pos;
	t_mat4x4	mat_cam_rot;
	t_vec3		target;
	t_vec3		look_dir;

	target = vec3(0, 0, 1);
	mat_cam_rot = mat_rotate_y(env->map.dir + env->player.keys.mouse.x / -5.0);
	look_dir = mat_mult_vec3(mat_cam_rot, target);
	next_pos = add_vec3(env->player.pos,
			mult_vec3(cross_product(vec3(0, 1, 0), look_dir),
				(float)env->delta_time / 100000.0));
	do_player_move(env, next_pos);
}

void	move_right(t_env *env)
{
	t_vec3		next_pos;
	t_mat4x4	mat_cam_rot;
	t_vec3		target;
	t_vec3		look_dir;

	target = vec3(0, 0, 1);
	mat_cam_rot = mat_rotate_y(env->map.dir + env->player.keys.mouse.x / -5.0);
	look_dir = mat_mult_vec3(mat_cam_rot, target);
	next_pos = sub_vec3(env->player.pos,
			mult_vec3(cross_product(vec3(0, 1, 0), look_dir),
				(float)env->delta_time / 100000.0));
	do_player_move(env, next_pos);
}

void	move_player(t_env *env)
{
	if (env->player.keys.w)
		move_forward(env);
	if (env->player.keys.s)
		move_backward(env);
	if (env->player.keys.a)
		move_left(env);
	if (env->player.keys.d)
		move_right(env);
}
