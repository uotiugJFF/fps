/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector3_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 23:30:13 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/22 17:55:55 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

float	dot_product(t_vec3 a, t_vec3 b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}

void	normalize_vec3(t_vec3 *v)
{
	float	len;

	len = sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
	v->x /= len;
	v->y /= len;
	v->z /= len;
}

t_vec3	get_look_dir(t_env *env)
{
	t_vec3		target;
	t_mat4x4	matrix_cam_rotate_y;

	target = vec3(0, 0, 1);
	matrix_cam_rotate_y = mat_rotate_y(env->map.dir
			+ env->player.keys.mouse.x / -5.0);
	target.y = env->player.keys.mouse.y / 100.0;
	return (mat_mult_vec3(matrix_cam_rotate_y, target));
}

t_vec3	get_target(t_env *env)
{
	t_vec3	target;

	target = add_vec3(env->player.pos, get_look_dir(env));
	return (target);
}

t_vec3	cross_product(t_vec3 a, t_vec3 b)
{
	t_vec3	r;

	r.x = a.y * b.z - a.z * b.y;
	r.y = a.z * b.x - a.x * b.z;
	r.z = a.x * b.y - a.y * b.x;
	return (r);
}
