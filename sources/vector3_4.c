/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector3_4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 03:05:05 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 20:11:00 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_vec3	rotate_y(t_vec3 p, int angle)
{
	t_vec3	rotated;
	float	rad;

	rad = deg2rad(angle);
	rotated.x = cos(rad) * p.x + sin(rad) * p.z;
	rotated.y = p.y;
	rotated.z = -sin(rad) * p.x + cos(rad) * p.z;
	return (rotated);
}

float	len_vec3(t_vec3 vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}
