/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh_floor.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 10:29:20 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/14 10:29:37 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	create_tri_floor_1(t_tile *tile, t_texture *tex)
{
	t_triangle	*tri_1;

	tri_1 = malloc(sizeof(t_triangle));
	if (!tri_1)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	tri_1->p[0] = vec3(tile->x * TILE, 0, -tile->z * TILE);
	tri_1->p[1] = vec3((tile->x + 1) * TILE, 0, -tile->z * TILE);
	tri_1->p[2] = vec3(tile->x * TILE, 0, -(tile->z + 1) * TILE);
	tri_1->t[0] = vec2(0, 0);
	tri_1->t[1] = vec2(1, 0);
	tri_1->t[2] = vec2(0, 1);
	tri_1->tex = tex;
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri_1));
}

void	create_tri_floor_2(t_tile *tile, t_texture *tex)
{
	t_triangle	*tri_2;

	tri_2 = malloc(sizeof(t_triangle));
	if (!tri_2)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	tri_2->p[0] = vec3((tile->x + 1) * TILE, 0, -tile->z * TILE);
	tri_2->p[1] = vec3((tile->x + 1) * TILE, 0, -(tile->z + 1) * TILE);
	tri_2->p[2] = vec3(tile->x * TILE, 0, -(tile->z + 1) * TILE);
	tri_2->t[0] = vec2(1, 0);
	tri_2->t[1] = vec2(1, 1);
	tri_2->t[2] = vec2(0, 1);
	tri_2->tex = tex;
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri_2));
}

void	add_floor(t_tile *tile, t_texture *tex)
{
	create_tri_floor_1(tile, tex);
	create_tri_floor_2(tile, tex);
}
