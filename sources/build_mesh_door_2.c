/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh_door_2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 14:37:28 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 14:37:41 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	rotate_door_left(t_tile *tile, t_door_mesh *dm)
{
	dm->s = rotate_y(dm->s, tile->door_angle + tile->door_dir);
	dm->e = rotate_y(dm->e, tile->door_angle + tile->door_dir);
	dm->s = add_vec3(dm->s, vec3(tile->x * TILE, 0, -(tile->z + 0.5) * TILE));
	dm->e = add_vec3(dm->e, vec3(tile->x * TILE, 0, -(tile->z + 0.5) * TILE));
	if (tile->door_dir == -90)
	{
		dm->s = add_vec3(dm->s, vec3(0.5 * TILE, 0, -0.5 * TILE));
		dm->e = add_vec3(dm->e, vec3(0.5 * TILE, 0, -0.5 * TILE));
	}
	else if (tile->door_dir == 90)
	{
		dm->s = add_vec3(dm->s, vec3(0.5 * TILE, 0, 0.5 * TILE));
		dm->e = add_vec3(dm->e, vec3(0.5 * TILE, 0, 0.5 * TILE));
	}
	else if (tile->door_dir == 180)
	{
		dm->s = add_vec3(dm->s, vec3(TILE, 0, 0));
		dm->e = add_vec3(dm->e, vec3(TILE, 0, 0));
	}
}

void	rotate_door_right(t_tile *tile, t_door_mesh *dm)
{
	dm->s = rotate_y(dm->s, 180 - tile->door_angle + tile->door_dir);
	dm->e = rotate_y(dm->e, 180 - tile->door_angle + tile->door_dir);
	dm->s = add_vec3(dm->s, vec3((tile->x + 1) * TILE, 0,
				-(tile->z + 0.5) * TILE));
	dm->e = add_vec3(dm->e, vec3((tile->x + 1) * TILE, 0,
				-(tile->z + 0.5) * TILE));
	if (tile->door_dir == -90)
	{
		dm->s = add_vec3(dm->s, vec3(-0.5 * TILE, 0, 0.5 * TILE));
		dm->e = add_vec3(dm->e, vec3(-0.5 * TILE, 0, 0.5 * TILE));
	}
	else if (tile->door_dir == 90)
	{
		dm->s = add_vec3(dm->s, vec3(-0.5 * TILE, 0, -0.5 * TILE));
		dm->e = add_vec3(dm->e, vec3(-0.5 * TILE, 0, -0.5 * TILE));
	}
	else if (tile->door_dir == 180)
	{
		dm->s = add_vec3(dm->s, vec3(-TILE, 0, 0));
		dm->e = add_vec3(dm->e, vec3(-TILE, 0, 0));
	}
}

void	build_door_tri_1(t_tile *tile, t_door_mesh dm, t_env *env)
{
	t_triangle	*tri;

	tri = malloc(sizeof(t_triangle));
	if (!tri)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	if (dm.pos == 'L')
		rotate_door_left(tile, &dm);
	else
		rotate_door_right(tile, &dm);
	tri->p[0] = vec3(dm.s.x, 0, dm.s.z);
	tri->p[1] = vec3(dm.e.x, HEIGHT, dm.e.z);
	tri->p[2] = vec3(dm.e.x, 0, dm.e.z);
	tri->t[0] = vec2(0, 1);
	tri->t[1] = vec2(0.5, 0);
	tri->t[2] = vec2(0.5, 1);
	tri->tex = env->tex_set.north;
	// memory leeak je pense
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri));
}

void	build_door_tri_2(t_tile *tile, t_door_mesh dm, t_env *env)
{
	t_triangle	*tri;

	tri = malloc(sizeof(t_triangle));
	if (!tri)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff;
	}
	if (dm.pos == 'L')
		rotate_door_left(tile, &dm);
	else
		rotate_door_right(tile, &dm);
	tri->p[0] = vec3(dm.s.x, 0, dm.s.z);
	tri->p[1] = vec3(dm.s.x, HEIGHT, dm.s.z);
	tri->p[2] = vec3(dm.e.x, HEIGHT, dm.e.z);
	tri->t[0] = vec2(0, 1);
	tri->t[1] = vec2(0, 0);
	tri->t[2] = vec2(0.5, 0);
	tri->tex = env->tex_set.north;
	// memory leak je pense
	ft_dl_add_back(&tile->mesh, ft_dl_new(tri));
}
