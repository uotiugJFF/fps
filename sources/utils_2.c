/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 12:42:09 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 02:34:09 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	toggle(int *v)
{
	if (*v)
		*v = 0;
	else
		*v = 1;
}

t_triangle	tri(t_vec3 p1, t_vec3 p2, t_vec3 p3)
{
	t_triangle	tr;

	tr.p[0] = p1;
	tr.p[1] = p2;
	tr.p[2] = p3;
	tr.tex = NULL;
	return (tr);
}

void	round_tri(t_triangle *tr)
{
	round_vec3(&tr->p[0]);
	round_vec3(&tr->p[1]);
	round_vec3(&tr->p[2]);
}

void	del_tri_dlist(void *data)
{
	free(data);
}

void	ft_swap(int *a, int *b)
{
	int	tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}
