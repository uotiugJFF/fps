/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh_door_1.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 14:26:17 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 14:37:19 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	set_door(t_door_mesh *dm, t_vec3 s, t_vec3 e, char pos)
{
	dm->s.x = s.x;
	dm->s.y = s.y;
	dm->s.z = s.z;
	dm->e.x = e.x;
	dm->e.y = e.y;
	dm->e.z = e.z;
	dm->pos = pos;
}

void	build_left_door(t_tile *tile, t_env *env)
{
	t_door_mesh	dm;

	set_door(&dm, vec3(0, 0, 0.0 * TILE),
		vec3(0.5 * TILE, 0, 0.0 * TILE), 'L');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0.5 * TILE, 0, 0.05 * TILE),
		vec3(0, 0, 0.05 * TILE), 'L');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0.5 * TILE, 0, 0.0 * TILE),
		vec3(0.5 * TILE, 0, 0.05 * TILE), 'L');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0, 0, 0.05 * TILE),
		vec3(0, 0, 0.0 * TILE), 'L');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
}

void	build_right_door(t_tile *tile, t_env *env)
{
	t_door_mesh	dm;

	set_door(&dm, vec3(0, 0, -0.05 * TILE),
		vec3(0.5 * TILE, 0, -0.05 * TILE), 'R');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0.5 * TILE, 0, 0 * TILE),
		vec3(0, 0, 0 * TILE), 'R');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0.5 * TILE, 0, -0.05 * TILE),
		vec3(0.5 * TILE, 0, 0 * TILE), 'R');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
	set_door(&dm, vec3(0, 0, 0 * TILE),
		vec3(0, 0, -0.05 * TILE), 'R');
	build_door_tri_1(tile, dm, env);
	build_door_tri_2(tile, dm, env);
}

void	set_door_rotation(t_tile *tile, t_env *env)
{
	if (tile->door_status == 0
		|| (tile->door_status == 3 && tile->door_angle <= 0))
	{
		tile->door_status = 0;
		tile->door_angle = 0;
	}
	if (tile->door_status == 1)
		tile->door_angle = minf(90, tile->door_angle
				+ env->delta_time / 5000.0);
	if (tile->door_status == 2
		|| (tile->door_status == 1 && tile->door_angle >= 90))
	{
		tile->door_status = 2;
		tile->door_angle = 90;
	}
	if (tile->door_status == 3)
		tile->door_angle = maxf(0, tile->door_angle - env->delta_time / 5000.0);
}

void	build_doors(t_tile *tile, t_env *env)
{
	add_floor_and_ceiling(tile, env);
	set_door_rotation(tile, env);
	build_left_door(tile, env);
	build_right_door(tile, env);
}
