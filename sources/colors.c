/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 00:29:18 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/17 00:29:36 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_rgb_clr	rgb(int r, int g, int b)
{
	t_rgb_clr	clr;

	clr.r = r;
	clr.g = g;
	clr.b = b;
	return (clr);
}

int	int_from_rgb(t_rgb_clr clr)
{
	int	r;
	int	g;
	int	b;

	r = min(max(clr.r, 0), 255) << 16;
	g = min(max(clr.g, 0), 255) << 8;
	b = min(max(clr.b, 0), 255);
	return (r + g + b);
}

t_rgb_clr	rgb_from_int(int clr)
{
	t_rgb_clr	converted_clr;

	converted_clr.r = (clr >> 16 & 0xFF);
	converted_clr.g = (clr >> 8 & 0xFF);
	converted_clr.b = (clr & 0xFF);
	return (converted_clr);
}
