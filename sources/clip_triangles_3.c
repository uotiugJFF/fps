/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clip_triangles_3.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:00:42 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:00:59 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

float	dist_to_plane(t_vec3 p, t_vec3 pl_n, t_vec3 pl_p)
{
	t_vec3	v;
	float	d;

	normalize_vec3(&pl_n);
	v = sub_vec3(p, pl_p);
	d = dot_product(v, pl_n);
	return (d);
}

void	init_clip_infos(t_clip_infos *inf, t_vec3 pl, t_vec3 norm,
		t_triangle tr)
{
	inf->pl = pl;
	inf->norm = norm;
	inf->tr = tr;
	inf->in = 0;
	inf->out = 0;
}

void	set_clip_infos_p1(t_clip_infos *infos)
{
	if (dist_to_plane(infos->tr.p[0], infos->norm, infos->pl) >= 0)
	{
		infos->in_pts[infos->in] = infos->tr.p[0];
		infos->in_tex[infos->in++] = infos->tr.t[0];
	}
	else
	{
		infos->out_pts[infos->out] = infos->tr.p[0];
		infos->out_tex[infos->out++] = infos->tr.t[0];
	}
	if (dist_to_plane(infos->tr.p[1], infos->norm, infos->pl) >= 0)
	{
		infos->in_pts[infos->in] = infos->tr.p[1];
		infos->in_tex[infos->in++] = infos->tr.t[1];
	}
	else
	{
		infos->out_pts[infos->out] = infos->tr.p[1];
		infos->out_tex[infos->out++] = infos->tr.t[1];
	}
}

void	set_clip_infos_p2(t_clip_infos *infos)
{
	if (dist_to_plane(infos->tr.p[2], infos->norm, infos->pl) >= 0)
	{
		infos->in_pts[infos->in] = infos->tr.p[2];
		infos->in_tex[infos->in++] = infos->tr.t[2];
	}
	else
	{
		infos->out_pts[infos->out] = infos->tr.p[2];
		infos->out_tex[infos->out++] = infos->tr.t[2];
	}
}

void	set_clip_infos(t_clip_infos *infos)
{
	set_clip_infos_p1(infos);
	set_clip_infos_p2(infos);
}
