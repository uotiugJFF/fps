/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_mesh_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 14:13:44 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/23 09:39:47 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	transform_view_tri(t_triangle *tr, t_env *env, t_mat4x4 view_matrix)
{
	t_vec3		normal;
	t_vec3		light;
	t_triangle	tri_view;
	t_dlist		*clipped_tr;

	normal = get_normal(tr);
	if (dot_product(normal, sub_vec3(tr->p[0], env->player.pos)) < 0)
	{
		light = mult_vec3(get_look_dir(env), -1);
		normalize_vec3(&light);
		tri_view.light = (dot_product(normal, light) + 1) / 2;
		tri_mat_mult_vec3(*tr, &tri_view, view_matrix);
		tri_view.t[0] = tr->t[0];
		tri_view.t[1] = tr->t[1];
		tri_view.t[2] = tr->t[2];
		tri_view.tex = tr->tex;
		clipped_tr = get_clip_tri(vec3(0, 0, NEAR_CLIP), vec3(0, 0, 1),
				tri_view);
		ft_dl_add_back(&env->map.clipped_mesh, clipped_tr);
	}
}

void	transform_enemies(t_env *env, t_mat4x4 view_matrix)
{
	t_dlist		*enemies_list;
	t_dlist		*mesh_list;
	t_enemy		*enemy;
	t_triangle	*tr;

	enemies_list = env->map.enemies;
	while (enemies_list)
	{
		enemy = enemies_list->data;
		mesh_list = enemy->mesh;
		while (mesh_list)
		{
			tr = mesh_list->data;
			transform_view_tri(tr, env, view_matrix);
			mesh_list = mesh_list->next;
		}
		enemies_list = enemies_list->next;
	}
}

void	transform_view(t_env *env, t_mat4x4 view_matrix)
{
	t_dlist		*tile_list;
	t_dlist		*mesh_list;
	t_triangle	*tr;

	tile_list = env->map.visible_mesh;
	while (tile_list)
	{
		mesh_list = ((t_tile *)(tile_list->data))->mesh;
		while (mesh_list)
		{
			tr = mesh_list->data;
			transform_view_tri(tr, env, view_matrix);
			mesh_list = mesh_list->next;
		}
		tile_list = tile_list->next;
	}
	build_enemies(env);
	transform_enemies(env, view_matrix);
}

void	transform_projection(t_env *env)
{
	t_dlist		*mesh_list;
	t_triangle	*tr;

	mesh_list = env->map.clipped_mesh;
	while (mesh_list)
	{
		tr = mesh_list->data;
		tri_mat_mult_vec3(*tr, tr, env->proj_mat);
		to_cartesian_space(tr);
		to_screen(tr);
		mesh_list = mesh_list->next;
	}
}

void	draw_triangle(t_triangle tr, int clr, t_env *env)
{
	draw_line(tr.p[0], tr.p[1], clr, env);
	draw_line(tr.p[1], tr.p[2], clr, env);
	draw_line(tr.p[2], tr.p[0], clr, env);
}

void	render_mesh(t_env *env)
{
	t_mat4x4	cam_matrix;
	t_mat4x4	view_matrix;

	ft_dl_clear(&env->map.clipped_mesh, &del_tri_dlist);
	cam_matrix = matrix_point_at(env->player.pos,
			get_target(env), vec3(0, 1, 0));
	view_matrix = matrix_quick_inverse(cam_matrix);
	transform_view(env, view_matrix);
	transform_projection(env);
	clip_bottom(env->map.clipped_mesh, env);
}
