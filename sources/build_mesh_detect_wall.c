/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_mesh_detect_wall.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 10:32:25 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 12:40:14 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	detect_left_wall(t_tile *tile, t_env *env)
{
	t_vec3	pts[2];

	if (env->map.map[tile->z][tile->x - 1].type != WALL)
	{
		pts[0] = vec3(tile->x * TILE, 0, -tile->z * TILE);
		pts[1] = vec3(tile->x * TILE, 0, -(tile->z + 1) * TILE);
		add_wall(tile, pts, env->tex_set.west);
	}
}

void	detect_right_wall(t_tile *tile, t_env *env)
{
	t_vec3	pts[2];

	if (env->map.map[tile->z][tile->x + 1].type != WALL)
	{
		pts[0] = vec3((tile->x + 1) * TILE, 0, -(tile->z + 1) * TILE);
		pts[1] = vec3((tile->x + 1) * TILE, 0, -tile->z * TILE);
		add_wall(tile, pts, env->tex_set.east);
	}
}

void	detect_top_wall(t_tile *tile, t_env *env)
{
	t_vec3	pts[2];

	if (env->map.map[tile->z - 1][tile->x].type != WALL)
	{
		pts[0] = vec3((tile->x + 1) * TILE, 0, -tile->z * TILE);
		pts[1] = vec3(tile->x * TILE, 0, -tile->z * TILE);
		add_wall(tile, pts, env->tex_set.north);
	}
}

void	detect_bottom_wall(t_tile *tile, t_env *env)
{
	t_vec3	pts[2];

	if (env->map.map[tile->z + 1][tile->x].type != WALL)
	{
		pts[0] = vec3(tile->x * TILE, 0, -(tile->z + 1) * TILE);
		pts[1] = vec3((tile->x + 1) * TILE, 0, -(tile->z + 1) * TILE);
		add_wall(tile, pts, env->tex_set.south);
	}
}
