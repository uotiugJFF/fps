/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_visible_mesh.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 11:19:51 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 10:46:53 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	reset_tile(void *data)
{
	((t_tile *)(data))->visible = 0;
}

void	add_visible_mesh(t_dda *dda, t_env *env)
{
	dda->wall_found = 0;
	dda->dist = 0;
	dda->max_dist = 100;
	while (!dda->wall_found && dda->dist < dda->max_dist)
	{
		if (dda->v_length.u < dda->v_length.v)
		{
			dda->map_check.u += dda->v_step.u;
			dda->dist = dda->v_length.u;
			dda->v_length.u += dda->v_unit_step.u;
		}
		else
		{
			dda->map_check.v += dda->v_step.v;
			dda->dist = dda->v_length.v;
			dda->v_length.v += dda->v_unit_step.v;
		}
		if (dda->map_check.u >= 0 && dda->map_check.u < env->map.x_max
			&& dda->map_check.v > 0 && dda->map_check.v <= env->map.z_max)
		{
			dda_check_ray(dda, env);
		}
	}
}

void	list_visible_mesh(t_env *env)
{
	t_dda	dda_c;
	t_dda	dda_l;
	t_dda	dda_r;
	float	start_angle;

	init_dda(&dda_c, 0, env);
	init_dda(&dda_l, -0.8, env);
	init_dda(&dda_r, 0.8, env);
	start_angle = -FOV * WIN_W / WIN_H / 2.0 - 10;
	while (start_angle < FOV * WIN_W / WIN_H / 2.0 + 10)
	{
		init_dda_ray(&dda_c, start_angle);
		init_dda_ray(&dda_l, start_angle);
		init_dda_ray(&dda_r, start_angle);
		add_visible_mesh(&dda_c, env);
		add_visible_mesh(&dda_l, env);
		add_visible_mesh(&dda_r, env);
		start_angle += (FOV + 20) / (WIN_W * 2);
	}
	build_enemies(env);
}
