/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clip_triangles_1.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 15:50:43 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/18 08:40:25 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_vec3	inter_plane(t_clip_infos inf, t_vec3 s, t_vec3 e, float *t)
{
	float	pl_d;
	float	ad;
	float	bd;
	t_vec3	line_s_to_e;
	t_vec3	line_to_inter;

	normalize_vec3(&inf.norm);
	pl_d = -dot_product(inf.norm, inf.pl);
	ad = dot_product(s, inf.norm);
	bd = dot_product(e, inf.norm);
	if (bd - ad == 0)
		return (e);
	*t = (-pl_d - ad) / (bd - ad);
	line_s_to_e = sub_vec3(e, s);
	line_to_inter = mult_vec3(line_s_to_e, *t);
	return (add_vec3(s, line_to_inter));
}

void	set_uvw(t_vec2 *tex, float t, t_clip_infos infos, t_tri_pts pt)
{
	tex->u = t * (infos.out_tex[pt.a].u - infos.in_tex[pt.b].u)
		+ infos.in_tex[pt.c].u;
	tex->v = t * (infos.out_tex[pt.a].v - infos.in_tex[pt.b].v)
		+ infos.in_tex[pt.c].v;
	tex->w = t * (infos.out_tex[pt.a].w - infos.in_tex[pt.b].w)
		+ infos.in_tex[pt.c].w;
}

t_tri_pts	pt(int a, int b, int c)
{
	t_tri_pts	pt;

	pt.a = a;
	pt.b = b;
	pt.c = c;
	return (pt);
}

t_dlist	*create_tri(t_clip_infos infos)
{
	if (infos.in == 0)
		return (NULL);
	else if (infos.in == 3)
		return (dup_triangle(infos.tr));
	else if (infos.in == 1 && infos.out == 2)
		return (create_tri_1_in(infos));
	else if (infos.in == 2 && infos.out == 1)
		return (create_tri_2_in(infos));
	return (NULL);
}

t_dlist	*get_clip_tri(t_vec3 pl, t_vec3 norm, t_triangle tr)
{
	t_clip_infos	infos;

	normalize_vec3(&norm);
	init_clip_infos(&infos, pl, norm, tr);
	set_clip_infos(&infos);
	return (create_tri(infos));
}
