/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/13 22:04:17 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 08:54:51 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	calc_map_size(t_map *map)
{
	int		x_max;
	int		z_max;
	t_dlist	*line;

	x_max = 0;
	z_max = 0;
	line = map->lines;
	while (line)
	{
		x_max = max(x_max, ((t_map_line *)(line->data))->x_max);
		line = line->next;
		z_max++;
	}
	map->x_max = x_max;
	map->z_max = z_max;
}

void	free_map_from(int i, t_map *map)
{
	while (i >= 0)
	{
		free(map->map[i]);
		i--;
	}
	free(map->map);
}

void	save_map(t_env *env, char *line, int z)
{
	int	x;

	x = -1;
	while (++x < env->map.x_max)
	{
		if (x < (int)ft_strlen(line))
		{
			if (line[x] == ' ' || line[x] == '1')
				wall_tile(x, z, env);
			else if (line[x] == 'P' || line[x] == 'O' || line[x] == 'I'
				|| line[x] == 'U')
				door_tile(x, z, env, line[x]);
			else
				empty_tile(x, z, env);
		}
		else
			wall_tile(x, z, env);
	}
}

void	save_map_lines(t_dlist *line, t_env *env)
{
	int	z;

	z = 0;
	while (line)
	{
		env->map.map[z] = malloc(sizeof(t_tile) * env->map.x_max);
		if (!env->map.map[z])
		{
			print_err(MALLOC_ERR);
			free_map_from(z, &env->map);
			// need to free some stuff...
			exit(MALLOC_ERR);
		}
		env->map.path[z] = malloc(sizeof(t_path_node) * env->map.x_max);
		if (!env->map.path[z])
		{
			print_err(MALLOC_ERR);
			//free_map_from(z, &env->map);
			// need to free some stuff...
			exit(MALLOC_ERR);
		}
		save_map(env, ((t_map_line *)(line->data))->str, z);
		line = line->next;
		z++;
	}
}

void	create_map(t_env *env)
{
	t_dlist	*line;

	calc_map_size(&env->map);
	env->map.map = malloc(sizeof(t_tile *) * env->map.z_max);
	if (!env->map.map)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff...
		exit(MALLOC_ERR);
	}
	env->map.path = malloc(sizeof(t_path_node *) * env->map.z_max);
	if (!env->map.path)
	{
		print_err(MALLOC_ERR);
		// need to free some stuff...
		exit(MALLOC_ERR);
	}
	line = env->map.lines;
	save_map_lines(line, env);
	ft_dl_clear(&env->map.lines, &del_map_line);
}
