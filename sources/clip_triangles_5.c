/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clip_triangles_5.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 08:59:39 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 09:02:12 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	*threaded_clipping(void *args)
{
	t_thread_args	*a;

	a = args;
	if (a->list)
		clip_top(a->list, a->env);
	return (NULL);
}

t_dlist	*thread_clip(t_dlist *tr, t_env *env)
{
	int	i;

	i = -1;
	while (++i < env->threads.clip_threads)
	{
		env->threads.clip_args[i].env = env;
		if (tr)
		{
			env->threads.clip_args[i].list = get_clip_tri(vec3(0, 0, 0),
					vec3(0, 1, 0), *((t_triangle *)tr->data));
			tr = tr->next;
		}
		else
			env->threads.clip_args[i].list = NULL;
		pthread_create(&env->threads.clip[i], NULL, &threaded_clipping,
			&env->threads.clip_args[i]);
	}
	i = -1;
	while (++i < env->threads.clip_threads)
	{
		pthread_join(env->threads.clip[i], NULL);
	}
	return (tr);
}
