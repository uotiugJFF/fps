/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/02 13:12:13 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/20 17:05:46 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	check_args(int ac, char **av)
{
	char		*ext;

	if (ac != 2)
		return (print_err(ARG_ERR));
	else
	{
		ext = ft_strrchr(av[1], '.');
		if (ft_strncmp(ext, ".cub", max(4, ft_strlen(ext))) != 0)
			return (print_err(ARG_ERR));
	}
	return (0);
}

int	main(int ac, char **av)
{
	t_env	env;

	if (check_args(ac, av) != 0)
		return (1);
	if (file_parser(av[1], &env) != 0)
		return (1);
	create_map(&env);
	build_mesh(&env);
	create_projection_matrix(&env);
	init_mlx(&env);
	return (0);
}
