/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clip_triangles_2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:35:24 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 09:02:00 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	draw_clipped_triangle(t_dlist *tr, t_env *env)
{
	t_dlist		*tmp;
	t_triangle	*tri;

	while (tr)
	{
		tri = tr->data;
		if (env->debug.light)
			fill_triangle(*tri, int_from_rgb(rgb(tri->light * 255,
						tri->light * 255, tri->light * 255)), env);
		else
			tex_triangle(*tri, env);
		if (env->debug.wire)
			draw_triangle(*tri, 0xff4444, env);
		tmp = tr;
		tr = tr->next;
		del_tri_dlist(tmp->data);
		free(tmp);
	}
}

void	clip_right(t_dlist *tr, t_env *env)
{
	t_dlist	*clipped;
	t_dlist	*tmp;

	while (tr)
	{
		clipped = get_clip_tri(vec3(WIN_W - 1, 0, 0),
				vec3(-1, 0, 0), *((t_triangle *)tr->data));
		draw_clipped_triangle(clipped, env);
		tmp = tr;
		tr = tr->next;
		del_tri_dlist(tmp->data);
		free(tmp);
	}
}

void	clip_left(t_dlist *tr, t_env *env)
{
	t_dlist	*clipped;
	t_dlist	*tmp;

	while (tr)
	{
		clipped = get_clip_tri(vec3(0, 0, 0), vec3(1, 0, 0),
				*((t_triangle *)tr->data));
		clip_right(clipped, env);
		tmp = tr;
		tr = tr->next;
		del_tri_dlist(tmp->data);
		free(tmp);
	}
}

void	clip_top(t_dlist *tr, t_env *env)
{
	t_dlist	*clipped;
	t_dlist	*tmp;

	while (tr)
	{
		clipped = get_clip_tri(vec3(0, WIN_H - 1, 0),
				vec3(0, -1, 0), *((t_triangle *)tr->data));
		clip_left(clipped, env);
		tmp = tr;
		tr = tr->next;
		del_tri_dlist(tmp->data);
		free(tmp);
	}
}

void	clip_bottom(t_dlist *tr, t_env *env)
{
	t_dlist			*clipped;

	while (tr)
	{
		if (env->debug.threads)
			tr = thread_clip(tr, env);
		else
		{
			clipped = get_clip_tri(vec3(0, 0, 0), vec3(0, 1, 0),
					*((t_triangle *)tr->data));
			clip_top(clipped, env);
			tr = tr->next;
		}
	}
}
