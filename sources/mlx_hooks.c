/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_hooks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/14 11:57:56 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 18:33:40 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

int	close_win(t_env *env)
{
	int	i;

	mlx_destroy_window(env->mlx, env->win);
	mlx_destroy_image(env->mlx, env->img.img);
	// need to destroy all textures here...
	mlx_destroy_display(env->mlx);
	i = -1;
	while (++i < WIN_W)
		pthread_mutex_destroy(&env->pxl_mutex[i]);
	free(env->mlx);
	exit(0);
}

int	release_key(int keycode, t_env *env)
{
	if (keycode == K_W)
		env->player.keys.w = 0;
	else if (keycode == K_S)
		env->player.keys.s = 0;
	else if (keycode == K_A)
		env->player.keys.a = 0;
	else if (keycode == K_D)
		env->player.keys.d = 0;
	return (0);
}

void	open_door(t_env *env)
{
	t_vec3	lookdir;
	int		x;
	int		z;

	x = env->player.pos.x / TILE;
	z = -env->player.pos.z / TILE;
	lookdir = get_look_dir(env);
	round_vec3(&lookdir);
	x += lookdir.x;
	z -= lookdir.z;
	if (env->map.map[z][x].type == DOOR && (env->map.map[z][x].door_status == 0
			|| env->map.map[z][x].door_status == 2))
		env->map.map[z][x].door_status
			= (env->map.map[z][x].door_status + 1) % 4;
}

void	check_key_press_1(int keycode, t_env *env)
{
	if (keycode == K_W)
		env->player.keys.w = 1;
	else if (keycode == K_S)
		env->player.keys.s = 1;
	else if (keycode == K_A)
		env->player.keys.a = 1;
	else if (keycode == K_D)
		env->player.keys.d = 1;
	else if (keycode == K_ESC)
		close_win(env);
	else if (keycode == K_E)
		open_door(env);
}

void	check_key_press_2(int keycode, t_env *env)
{
	if (keycode == K_F1)
		toggle(&env->debug.dda);
	else if (keycode == K_F2)
		toggle(&env->debug.z);
	else if (keycode == K_F3)
		toggle(&env->debug.uv);
	else if (keycode == K_F4)
		toggle(&env->debug.light);
	else if (keycode == K_F5)
		toggle(&env->debug.wire);
	else if (keycode == K_F6)
		toggle(&env->debug.threads);
	else if (keycode == K_F7)
		env->threads.clip_threads = max(2, env->threads.clip_threads - 1);
	else if (keycode == K_F8)
		env->threads.clip_threads = min(300, env->threads.clip_threads + 1);
	else if (keycode == K_F12)
		toggle(&env->debug.infos);
}

int	press_key(int keycode, t_env *env)
{
	check_key_press_1(keycode, env);
	check_key_press_2(keycode, env);
	return (0);
}

void	set_death_anim_enemy(t_enemy *enemy)
{
	enemy->cur_anim = 11;
	enemy->dead = 1;
	enemy->death_time = get_timestamp();
}

void	check_shoot_enemies(t_vec3 lookdir, t_env *env)
{
	t_vec3	enemy_dir;
	t_vec3	enemy_player;
	t_dlist	*enemies_list;
	t_enemy	*enemy;
	float	shoot_angle_v;

	enemies_list = env->map.enemies;
	while (enemies_list)
	{
		enemy = enemies_list->data;
		if (enemy->visible)
		{
			enemy_dir = sub_vec3(vec3(enemy->pos.x, 0, enemy->pos.z),
					vec3(env->player.pos.x, 0, env->player.pos.z));
			enemy_player = enemy_dir;
			normalize_vec3(&enemy_dir);
			if (dot_product(enemy_dir, lookdir) > 0.995)
			{
				shoot_angle_v = (len_vec3(enemy_player)
						* env->player.keys.mouse.y) / 100;
				if (shoot_angle_v > -2 && shoot_angle_v < 1)
					set_death_anim_enemy(enemy);
			}
		}
		enemies_list = enemies_list->next;
	}
}

int	mouse_hook(int btn, int x, int y, void *e)
{
	t_env	*env;
	t_vec3	target;
	t_vec3	lookdir;

	(void)x;
	(void)y;
	env = e;
	target = get_target(env);
	lookdir = sub_vec3(target, env->player.pos);
	lookdir.y = 0;
	if (get_timestamp() - env->player.shoot_time > 700000)
		env->player.shoot = 0;
	if (!env->player.shoot)
	{
		env->player.shoot = 1;
		env->player.shoot_time = get_timestamp();
		check_shoot_enemies(lookdir, env);
	}
	return (btn);
}
