/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug_infos.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/21 09:08:08 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/21 09:11:09 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	debug_infos(long t1, long t2, t_env *env)
{
	if (env->debug.infos)
	{
		mlx_string_put(env->mlx, env->win, 10, 15, 0xFF4444, "FPS :");
		mlx_string_put(env->mlx, env->win, 45, 15, 0xFF4444,
			ft_itoa(get_fps(t1, t2)));
		mlx_string_put(env->mlx, env->win, 10, 30, 0xFF4444, "CLIP THREADS :");
		mlx_string_put(env->mlx, env->win, 100, 30, 0xFF4444,
			ft_itoa(env->threads.clip_threads));
	}
}
