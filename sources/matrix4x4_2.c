/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix4x4_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/16 23:37:39 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/16 23:37:52 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

t_mat4x4	mat_rotate_x(float angle)
{
	t_mat4x4	mat;
	float		rad;

	matrix_init(&mat);
	rad = deg2rad(angle);
	mat.m[0][0] = 1;
	mat.m[1][1] = cos(rad);
	mat.m[1][2] = sin(rad);
	mat.m[2][1] = -sin(rad);
	mat.m[2][2] = cos(rad);
	mat.m[3][3] = 1;
	return (mat);
}

t_mat4x4	mat_rotate_y(float angle)
{
	t_mat4x4	mat;
	float		rad;

	matrix_init(&mat);
	rad = deg2rad(angle);
	mat.m[0][0] = cos(rad);
	mat.m[0][2] = sin(rad);
	mat.m[2][0] = -sin(rad);
	mat.m[1][1] = 1;
	mat.m[2][2] = cos(rad);
	mat.m[3][3] = 1;
	return (mat);
}

t_mat4x4	mat_rotate_z(float angle)
{
	t_mat4x4	mat;
	float		rad;

	matrix_init(&mat);
	rad = deg2rad(angle);
	mat.m[0][0] = cos(rad);
	mat.m[0][1] = sin(rad);
	mat.m[1][0] = -sin(rad);
	mat.m[1][1] = cos(rad);
	mat.m[2][2] = 1;
	mat.m[3][3] = 1;
	return (mat);
}
