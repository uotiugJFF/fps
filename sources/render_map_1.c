/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render_map_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/17 20:10:31 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/27 11:51:14 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	sub_player_map(t_triangle *t1, t_triangle *t2, t_env *env)
{
	t1->p[0] = sub_vec3(t1->p[0], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
	t1->p[1] = sub_vec3(t1->p[1], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
	t1->p[2] = sub_vec3(t1->p[2], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
	t2->p[0] = sub_vec3(t2->p[0], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
	t2->p[1] = sub_vec3(t2->p[1], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
	t2->p[2] = sub_vec3(t2->p[2], vec3(env->player.pos.x * 10 / TILE,
				env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
}

void	rotate_map(t_triangle *t1, t_triangle *t2, t_env *env)
{
	t1->p[0] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t1->p[0]);
	t1->p[1] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t1->p[1]);
	t1->p[2] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t1->p[2]);
	t2->p[0] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t2->p[0]);
	t2->p[1] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t2->p[1]);
	t2->p[2] = mat_mult_vec3(mat_rotate_z(-env->map.dir
				+ env->player.keys.mouse.x / 5.0), t2->p[2]);
}

void	move_map(t_triangle *t1, t_triangle *t2)
{
	t1->p[0] = add_vec3(t1->p[0], vec3(MAP_POS_X, MAP_POS_Y, 0));
	t1->p[1] = add_vec3(t1->p[1], vec3(MAP_POS_X, MAP_POS_Y, 0));
	t1->p[2] = add_vec3(t1->p[2], vec3(MAP_POS_X, MAP_POS_Y, 0));
	t2->p[0] = add_vec3(t2->p[0], vec3(MAP_POS_X, MAP_POS_Y, 0));
	t2->p[1] = add_vec3(t2->p[1], vec3(MAP_POS_X, MAP_POS_Y, 0));
	t2->p[2] = add_vec3(t2->p[2], vec3(MAP_POS_X, MAP_POS_Y, 0));
}

void	render_map_tile(t_tile *t, t_env *env)
{
	int			clr;
	t_triangle	t1;
	t_triangle	t2;

	if (t->type == WALL)
		return ;
	if (t->type == EMPTY)
		clr = 0xAAAAAA;
	else if (t->type == DOOR && t->door_status == 2)
		clr = 0xAAAAFF;
	else
		clr = 0xFFAAAA;
	t1 = tri(vec3(t->x * 10, (env->map.z_max - 1 - t->z) * 10, 0),
			vec3(t->x * 10, (env->map.z_max - 1 - t->z + 1) * 10, 0),
			vec3((t->x + 1) * 10, (env->map.z_max - 1 - t->z + 1) * 10, 0));
	t2 = tri(vec3(t->x * 10, (env->map.z_max - 1 - t->z) * 10, 0),
			vec3((t->x + 1) * 10, (env->map.z_max - 1 - t->z + 1) * 10, 0),
			vec3((t->x + 1) * 10, (env->map.z_max - 1 - t->z) * 10, 0));
	sub_player_map(&t1, &t2, env);
	rotate_map(&t1, &t2, env);
	move_map(&t1, &t2);
	fill_triangle(t1, clr, env);
	fill_triangle(t2, clr, env);
}

void	draw_entity(int x, int y, int clr, t_env *env)
{
	int	pxl_x;
	int	pxl_y;

	pxl_x = x - 3;
	while (++pxl_x < x + 3)
	{
		pxl_y = y - 3;
		while (++pxl_y < y + 3)
			put_pxl(env, pxl_x, pxl_y, clr);
	}
}

void	render_map_enemies(t_env *env)
{
	t_dlist	*enemies_list;
	t_enemy	*enemy;
	t_vec3	pos;

	enemies_list = env->map.enemies;
	while (enemies_list)
	{
		enemy = enemies_list->data;
		if (enemy->visible)
		{
			pos.x = enemy->pos.x / TILE * 10;
			pos.y = (env->map.z_max + (enemy->pos.z / TILE)) * 10;
			pos = sub_vec3(pos, vec3(env->player.pos.x * 10 / TILE,
						env->player.pos.z * 10 / TILE + env->map.z_max * 10, 0));
			pos = mat_mult_vec3(mat_rotate_z(-env->map.dir
						+ env->player.keys.mouse.x / 5.0), pos);
			pos = add_vec3(pos, vec3(MAP_POS_X, MAP_POS_Y, 0));
			draw_entity(pos.x, pos.y, 0xFF5555, env);
		}
		enemies_list = enemies_list->next;
	}
}

void	render_map(t_env *env)
{
	int	x;
	int	z;

	z = -1;
	while (++z < env->map.z_max)
	{
		x = -1;
		while (++x < env->map.x_max)
		{
			render_map_tile(&env->map.map[z][x], env);
		}
	}
	render_map_enemies(env);
	draw_entity(MAP_POS_X, MAP_POS_Y, 0x55FF55, env);
}
