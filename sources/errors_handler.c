/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors_handler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/07 00:56:41 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/14 11:13:28 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fps.h"

void	print_err_pt1(int err)
{
	if (err == ARG_ERR)
	{
		ft_putstr_fd("Please add only the path of the *.cub file after the", 2);
		ft_putstr_fd(" program name.\nExample : ./FPS resources/map.cub\n", 2);
	}
	else if (err == MAP_PATH_ERR)
	{
		ft_putstr_fd("Program unable to read the *.cub file.\nPlease check", 2);
		ft_putstr_fd(" that is a valid file/path.\n", 2);
	}
	else if (err == MAP_EMPTY_LINE_ERR)
	{
		ft_putstr_fd("There is an error with the map.\nNo empty line allow", 2);
		ft_putstr_fd("ed within the map.\n", 2);
	}
	else if (err == MAP_ARGS_ERR)
	{
		ft_putstr_fd("Informations missing.\nPlease check if all textures ", 2);
		ft_putstr_fd("informations are in the *.cub file.\n", 2);
	}
}

void	print_err_pt2(int err)
{
	if (err == MAP_CHAR)
	{
		ft_putstr_fd("Character(s) not allowed in the map.\nOnly N, S, W, ", 2);
		ft_putstr_fd("E, 0 and 1 are allowed\n", 2);
	}
	else if (err == MAP_NO_START)
		ft_putstr_fd("The map has no starting point.\n", 2);
	else if (err == MAP_NOT_CLOSED)
		ft_putstr_fd("The map is not closed.\n", 2);
	else if (err == MALLOC_ERR)
		ft_putstr_fd("A malloc() failed.\n", 2);
	else if (err == MLX_INIT_ERR)
		ft_putstr_fd("Something went wrong initializing MinilibX.\n", 2);
	else if (err == MLX_WIN_ERR)
		ft_putstr_fd("Something went wrong initializing MinilibX window.\n", 2);
	else if (err == MLX_IMG_ERR)
		ft_putstr_fd("Something went wrong initializing MinilibX image.\n", 2);
	else if (err > MLX_IMG_ERR)
		ft_putstr_fd("UNKNOWN ERROR\n", 2);
}

int	print_err(int err)
{
	ft_putstr_fd("Error.\n", 2);
	print_err_pt1(err);
	print_err_pt2(err);
	return (err);
}
