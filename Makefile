# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <guill@umebrunet.fr>               +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/12/02 12:33:28 by gbrunet           #+#    #+#              #
#    Updated: 2023/12/23 08:46:25 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = FPS

CC = cc

CFLAGS = -Wall -Wextra -Werror -I$(X11)

LIBFT = libft_2.0

MINILIBX = minilibx

X11 = /usr/X11/include

X11LIB = $(X11)/../lib

INCLUDES = includes

SRC_DIR = sources/

OBJ_DIR = objects/

SRC_FILES = main \
			file_parser_1 file_parser_2 file_parser_3 \
			closed_map_1 closed_map_2 create_map_1 create_map_2 \
			build_mesh build_mesh_wall build_mesh_floor build_mesh_ceiling \
			build_mesh_detect_wall build_mesh_door_1 build_mesh_door_2 \
			build_enemies \
			errors_handler utils_1 utils_2 utils_3 \
			init_mlx_1 init_mlx_2 mlx_hooks \
			moves_calculation move_player \
			list_visible_mesh dda_calculation \
			clip_triangles_1 clip_triangles_2 clip_triangles_3 \
			clip_triangles_4 clip_triangles_5 \
			debug_dda_1 debug_dda_2 debug_enemy debug_infos \
			render_1 render_2 render_mesh_1 render_mesh_2 \
			keyframes \
			render_map_1 render_map_2 \
			draw_line colors \
			fill_triangle_1 fill_triangle_2 \
			tex_triangle_1 tex_triangle_2 tex_triangle_3 \
			vector3_1 vector3_2 vector3_3 vector3_4 vector2 \
			matrix4x4_1 matrix4x4_2 \
			a_star_1 a_star_2 a_star_3

SRC = $(addprefix $(SRC_DIR), $(addsuffix .c, $(SRC_FILES)))

OBJ = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC_FILES)))

$(OBJ_DIR)%.o : $(SRC_DIR)%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

MLX = -lmlx -L $(MINILIBX) -lXext -lX11 -lm

.PHONY : all clean fclean re norme

all : $(NAME)

lib :
	make -C $(LIBFT)
	make -C $(MINILIBX)

$(NAME) : lib $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L $(X11LIB) $(MLX) -lft -L $(LIBFT) \
		-lpthread

ok : $(OBJ)

clean :
	make clean -C $(LIBFT)
	make clean -C $(MINILIBX)
	$(RM) -rf $(OBJ_DIR)

fclean : clean
	make fclean -C $(LIBFT)
	$(RM) $(NAME)

re : fclean all

norme :
	@norminette $(SRC) | grep -v Norme -B1 || true
	@norminette $(INCLUDES) -R CheckDefine | grep -v Norme -B1 || true
